﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cubiculos
{
    public partial class ValidaEncargado : Form
    {

        Form1 principal;
        int cubiculo;
        public ValidaEncargado(Form1 principal, int cubiculo)
        {
            InitializeComponent();
            this.principal = principal;
            this.cubiculo = cubiculo;
            if (cubiculo != 0)
            {
                label1.Text = "Ingrese su numero de control";
            }
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            if(cubiculo == 0)
            {
                if (principal.validaEncargado(txtPass.Text))
                {
                    this.DialogResult = DialogResult.OK;
                }
                else
                {
                    MessageBox.Show("La contraseña es incorrecta. \n Es necesario que el encargado de turno introduzca su contraseña.");
                    this.DialogResult = DialogResult.Cancel;
                }
            }
            else
            {
                try
                {
                    PrestamoNodo P = principal.cabeza;
                    if (P.cubiculo == cubiculo)
                    {
                        if (P.id_alumno == Convert.ToInt32(txtPass.Text) || principal.validaEncargado(txtPass.Text))
                        {
                            this.DialogResult = DialogResult.OK;
                        }
                        else
                        {
                            MessageBox.Show("La contraseña es incorrecta. \n Es necesario que el encargado de turno introduzca su contraseña.");
                            this.DialogResult = DialogResult.Cancel;
                        }
                    }
                    else
                    {
                        while (P.sig != null)
                        {
                            P = P.sig;
                            if (P.cubiculo == cubiculo)
                            {
                                if (P.id_alumno == Convert.ToInt32(txtPass.Text) || principal.validaEncargado(txtPass.Text))
                                {
                                    this.DialogResult = DialogResult.OK;
                                }
                                else
                                {
                                    MessageBox.Show("La contraseña es incorrecta. \n Es necesario que el encargado de turno introduzca su contraseña.");
                                    this.DialogResult = DialogResult.Cancel;
                                }
                            }
                        }
                    }
                }
                catch
                {
                    MessageBox.Show("Su numero de control no coincide");
                    this.DialogResult = DialogResult.Cancel;
                }
            }
        }

        private void txtPass_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnConfirmar_Click(this, new EventArgs());
            }
        }
    }
}
