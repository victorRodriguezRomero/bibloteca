﻿namespace Cubiculos
{
    partial class Administra_Encargados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbC_encargado = new System.Windows.Forms.TabControl();
            this.tbP_ae = new System.Windows.Forms.TabPage();
            this.txt_pass2 = new System.Windows.Forms.TextBox();
            this.lbl_pass2 = new System.Windows.Forms.Label();
            this.txt_ide = new System.Windows.Forms.TextBox();
            this.lbl_ide = new System.Windows.Forms.Label();
            this.btnA_salir = new System.Windows.Forms.Button();
            this.btnA_agregar = new System.Windows.Forms.Button();
            this.cBA_nivel = new System.Windows.Forms.ComboBox();
            this.tBA_pass = new System.Windows.Forms.TextBox();
            this.tBA_nombre = new System.Windows.Forms.TextBox();
            this.lblA_nivel = new System.Windows.Forms.Label();
            this.lblA_pass = new System.Windows.Forms.Label();
            this.lblA_nombre = new System.Windows.Forms.Label();
            this.tbP_me = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.txtNombreModifica = new System.Windows.Forms.TextBox();
            this.lblControl = new System.Windows.Forms.Label();
            this.btnModificar = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tbP_ee = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bntLimpiar = new System.Windows.Forms.Button();
            this.lblconE = new System.Windows.Forms.Label();
            this.lblNombreE = new System.Windows.Forms.Label();
            this.txtControl = new System.Windows.Forms.TextBox();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_elime = new System.Windows.Forms.Button();
            this.tbC_encargado.SuspendLayout();
            this.tbP_ae.SuspendLayout();
            this.tbP_me.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tbP_ee.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbC_encargado
            // 
            this.tbC_encargado.Controls.Add(this.tbP_ae);
            this.tbC_encargado.Controls.Add(this.tbP_me);
            this.tbC_encargado.Controls.Add(this.tbP_ee);
            this.tbC_encargado.Location = new System.Drawing.Point(12, 12);
            this.tbC_encargado.Name = "tbC_encargado";
            this.tbC_encargado.SelectedIndex = 0;
            this.tbC_encargado.Size = new System.Drawing.Size(526, 384);
            this.tbC_encargado.TabIndex = 21;
            // 
            // tbP_ae
            // 
            this.tbP_ae.Controls.Add(this.txt_pass2);
            this.tbP_ae.Controls.Add(this.lbl_pass2);
            this.tbP_ae.Controls.Add(this.txt_ide);
            this.tbP_ae.Controls.Add(this.lbl_ide);
            this.tbP_ae.Controls.Add(this.btnA_salir);
            this.tbP_ae.Controls.Add(this.btnA_agregar);
            this.tbP_ae.Controls.Add(this.cBA_nivel);
            this.tbP_ae.Controls.Add(this.tBA_pass);
            this.tbP_ae.Controls.Add(this.tBA_nombre);
            this.tbP_ae.Controls.Add(this.lblA_nivel);
            this.tbP_ae.Controls.Add(this.lblA_pass);
            this.tbP_ae.Controls.Add(this.lblA_nombre);
            this.tbP_ae.Location = new System.Drawing.Point(4, 22);
            this.tbP_ae.Name = "tbP_ae";
            this.tbP_ae.Padding = new System.Windows.Forms.Padding(3);
            this.tbP_ae.Size = new System.Drawing.Size(518, 358);
            this.tbP_ae.TabIndex = 0;
            this.tbP_ae.Text = "Agregar";
            this.tbP_ae.UseVisualStyleBackColor = true;
            // 
            // txt_pass2
            // 
            this.txt_pass2.Location = new System.Drawing.Point(245, 148);
            this.txt_pass2.Name = "txt_pass2";
            this.txt_pass2.PasswordChar = '*';
            this.txt_pass2.Size = new System.Drawing.Size(100, 20);
            this.txt_pass2.TabIndex = 3;
            this.txt_pass2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_ide_KeyDown);
            // 
            // lbl_pass2
            // 
            this.lbl_pass2.AutoSize = true;
            this.lbl_pass2.Location = new System.Drawing.Point(126, 151);
            this.lbl_pass2.Name = "lbl_pass2";
            this.lbl_pass2.Size = new System.Drawing.Size(121, 13);
            this.lbl_pass2.TabIndex = 15;
            this.lbl_pass2.Text = "Confirme la contraseña: ";
            // 
            // txt_ide
            // 
            this.txt_ide.Location = new System.Drawing.Point(246, 46);
            this.txt_ide.Name = "txt_ide";
            this.txt_ide.Size = new System.Drawing.Size(100, 20);
            this.txt_ide.TabIndex = 0;
            this.txt_ide.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_ide_KeyDown);
            // 
            // lbl_ide
            // 
            this.lbl_ide.AutoSize = true;
            this.lbl_ide.Location = new System.Drawing.Point(151, 49);
            this.lbl_ide.Name = "lbl_ide";
            this.lbl_ide.Size = new System.Drawing.Size(96, 13);
            this.lbl_ide.TabIndex = 13;
            this.lbl_ide.Text = "ID del Encargado: ";
            // 
            // btnA_salir
            // 
            this.btnA_salir.Location = new System.Drawing.Point(271, 222);
            this.btnA_salir.Name = "btnA_salir";
            this.btnA_salir.Size = new System.Drawing.Size(75, 23);
            this.btnA_salir.TabIndex = 6;
            this.btnA_salir.Text = "Salir";
            this.btnA_salir.UseVisualStyleBackColor = true;
            this.btnA_salir.Click += new System.EventHandler(this.btnA_salir_Click_1);
            // 
            // btnA_agregar
            // 
            this.btnA_agregar.Location = new System.Drawing.Point(173, 222);
            this.btnA_agregar.Name = "btnA_agregar";
            this.btnA_agregar.Size = new System.Drawing.Size(75, 23);
            this.btnA_agregar.TabIndex = 5;
            this.btnA_agregar.Text = "Agregar";
            this.btnA_agregar.UseVisualStyleBackColor = true;
            this.btnA_agregar.Click += new System.EventHandler(this.btnA_agregar_Click_1);
            // 
            // cBA_nivel
            // 
            this.cBA_nivel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBA_nivel.FormattingEnabled = true;
            this.cBA_nivel.Items.AddRange(new object[] {
            "1",
            "2"});
            this.cBA_nivel.Location = new System.Drawing.Point(246, 178);
            this.cBA_nivel.MaxDropDownItems = 2;
            this.cBA_nivel.Name = "cBA_nivel";
            this.cBA_nivel.Size = new System.Drawing.Size(100, 21);
            this.cBA_nivel.TabIndex = 4;
            this.cBA_nivel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_ide_KeyDown);
            // 
            // tBA_pass
            // 
            this.tBA_pass.Location = new System.Drawing.Point(246, 118);
            this.tBA_pass.Name = "tBA_pass";
            this.tBA_pass.PasswordChar = '*';
            this.tBA_pass.Size = new System.Drawing.Size(100, 20);
            this.tBA_pass.TabIndex = 2;
            this.tBA_pass.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_ide_KeyDown);
            // 
            // tBA_nombre
            // 
            this.tBA_nombre.Location = new System.Drawing.Point(246, 82);
            this.tBA_nombre.Name = "tBA_nombre";
            this.tBA_nombre.Size = new System.Drawing.Size(100, 20);
            this.tBA_nombre.TabIndex = 1;
            this.tBA_nombre.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_ide_KeyDown);
            // 
            // lblA_nivel
            // 
            this.lblA_nivel.AutoSize = true;
            this.lblA_nivel.Location = new System.Drawing.Point(157, 181);
            this.lblA_nivel.Name = "lblA_nivel";
            this.lblA_nivel.Size = new System.Drawing.Size(91, 13);
            this.lblA_nivel.TabIndex = 10;
            this.lblA_nivel.Text = "Nivel de Acceso: ";
            // 
            // lblA_pass
            // 
            this.lblA_pass.AutoSize = true;
            this.lblA_pass.Location = new System.Drawing.Point(180, 121);
            this.lblA_pass.Name = "lblA_pass";
            this.lblA_pass.Size = new System.Drawing.Size(67, 13);
            this.lblA_pass.TabIndex = 9;
            this.lblA_pass.Text = "Contraseña: ";
            // 
            // lblA_nombre
            // 
            this.lblA_nombre.AutoSize = true;
            this.lblA_nombre.Location = new System.Drawing.Point(126, 85);
            this.lblA_nombre.Name = "lblA_nombre";
            this.lblA_nombre.Size = new System.Drawing.Size(122, 13);
            this.lblA_nombre.TabIndex = 8;
            this.lblA_nombre.Text = "Nombre del Encargado: ";
            // 
            // tbP_me
            // 
            this.tbP_me.Controls.Add(this.groupBox3);
            this.tbP_me.Controls.Add(this.groupBox2);
            this.tbP_me.Location = new System.Drawing.Point(4, 22);
            this.tbP_me.Name = "tbP_me";
            this.tbP_me.Padding = new System.Windows.Forms.Padding(3);
            this.tbP_me.Size = new System.Drawing.Size(518, 358);
            this.tbP_me.TabIndex = 1;
            this.tbP_me.Text = "Modificar";
            this.tbP_me.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button4);
            this.groupBox3.Controls.Add(this.textBox5);
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.textBox1);
            this.groupBox3.Controls.Add(this.button3);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Location = new System.Drawing.Point(66, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(351, 131);
            this.groupBox3.TabIndex = 22;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Encargado";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(272, 92);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "Salir";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(147, 93);
            this.textBox5.Name = "textBox5";
            this.textBox5.PasswordChar = '*';
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(100, 20);
            this.textBox5.TabIndex = 14;
            this.textBox5.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(329, 61);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(18, 27);
            this.button1.TabIndex = 2;
            this.button1.Text = "<";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(144, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "__________________";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(143, 35);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(117, 20);
            this.textBox1.TabIndex = 0;
            this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(272, 33);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 1;
            this.button3.Text = "Buscar";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(136, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Contrasena del Encargado:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 67);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Nombre del Encargado:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(45, 38);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "ID del Encargado:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox4);
            this.groupBox2.Controls.Add(this.textBox3);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.textBox2);
            this.groupBox2.Controls.Add(this.txtNombreModifica);
            this.groupBox2.Controls.Add(this.lblControl);
            this.groupBox2.Controls.Add(this.btnModificar);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Location = new System.Drawing.Point(66, 143);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(351, 200);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Modificar";
            this.groupBox2.Visible = false;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(140, 151);
            this.textBox4.Name = "textBox4";
            this.textBox4.PasswordChar = '*';
            this.textBox4.Size = new System.Drawing.Size(100, 20);
            this.textBox4.TabIndex = 7;
            this.textBox4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNombreModifica_KeyDown);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(140, 123);
            this.textBox3.Name = "textBox3";
            this.textBox3.PasswordChar = '*';
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 6;
            this.textBox3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNombreModifica_KeyDown);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(0, 154);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(111, 13);
            this.label13.TabIndex = 11;
            this.label13.Text = "Confirmar Contrasena:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(13, 126);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(98, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "Nueva contrasena:";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(140, 97);
            this.textBox2.Name = "textBox2";
            this.textBox2.PasswordChar = '*';
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 5;
            this.textBox2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNombreModifica_KeyDown);
            // 
            // txtNombreModifica
            // 
            this.txtNombreModifica.Location = new System.Drawing.Point(140, 68);
            this.txtNombreModifica.Name = "txtNombreModifica";
            this.txtNombreModifica.Size = new System.Drawing.Size(205, 20);
            this.txtNombreModifica.TabIndex = 4;
            this.txtNombreModifica.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNombreModifica_KeyDown);
            // 
            // lblControl
            // 
            this.lblControl.AutoSize = true;
            this.lblControl.Location = new System.Drawing.Point(137, 43);
            this.lblControl.Name = "lblControl";
            this.lblControl.Size = new System.Drawing.Size(18, 13);
            this.lblControl.TabIndex = 7;
            this.lblControl.Text = "ID";
            // 
            // btnModificar
            // 
            this.btnModificar.Location = new System.Drawing.Point(270, 171);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(75, 23);
            this.btnModificar.TabIndex = 8;
            this.btnModificar.Text = "Modificar";
            this.btnModificar.UseVisualStyleBackColor = true;
            this.btnModificar.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 100);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(97, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Contrasena Actual:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(64, 71);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Nombre:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(35, 43);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Numero de ID:";
            // 
            // tbP_ee
            // 
            this.tbP_ee.Controls.Add(this.button2);
            this.tbP_ee.Controls.Add(this.groupBox1);
            this.tbP_ee.Controls.Add(this.btn_elime);
            this.tbP_ee.Location = new System.Drawing.Point(4, 22);
            this.tbP_ee.Name = "tbP_ee";
            this.tbP_ee.Padding = new System.Windows.Forms.Padding(3);
            this.tbP_ee.Size = new System.Drawing.Size(518, 358);
            this.tbP_ee.TabIndex = 2;
            this.tbP_ee.Text = "Eliminar";
            this.tbP_ee.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(243, 247);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "Salir";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bntLimpiar);
            this.groupBox1.Controls.Add(this.lblconE);
            this.groupBox1.Controls.Add(this.lblNombreE);
            this.groupBox1.Controls.Add(this.txtControl);
            this.groupBox1.Controls.Add(this.btnBuscar);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(71, 77);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(351, 131);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Encargado";
            // 
            // bntLimpiar
            // 
            this.bntLimpiar.Location = new System.Drawing.Point(329, 62);
            this.bntLimpiar.Name = "bntLimpiar";
            this.bntLimpiar.Size = new System.Drawing.Size(18, 27);
            this.bntLimpiar.TabIndex = 2;
            this.bntLimpiar.Text = "<";
            this.bntLimpiar.UseVisualStyleBackColor = true;
            this.bntLimpiar.Click += new System.EventHandler(this.bntLimpiar_Click_1);
            // 
            // lblconE
            // 
            this.lblconE.AutoSize = true;
            this.lblconE.Location = new System.Drawing.Point(144, 96);
            this.lblconE.Name = "lblconE";
            this.lblconE.Size = new System.Drawing.Size(115, 13);
            this.lblconE.TabIndex = 0;
            this.lblconE.Text = "__________________";
            // 
            // lblNombreE
            // 
            this.lblNombreE.AutoSize = true;
            this.lblNombreE.Location = new System.Drawing.Point(144, 69);
            this.lblNombreE.Name = "lblNombreE";
            this.lblNombreE.Size = new System.Drawing.Size(115, 13);
            this.lblNombreE.TabIndex = 0;
            this.lblNombreE.Text = "__________________";
            // 
            // txtControl
            // 
            this.txtControl.Location = new System.Drawing.Point(143, 35);
            this.txtControl.Name = "txtControl";
            this.txtControl.Size = new System.Drawing.Size(117, 20);
            this.txtControl.TabIndex = 0;
            this.txtControl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtControl_KeyDown);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(272, 33);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 23);
            this.btnBuscar.TabIndex = 1;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 96);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(136, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Contrasena del Encargado:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 67);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Nombre del Encargado:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(45, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "ID del Encargado:";
            // 
            // btn_elime
            // 
            this.btn_elime.Location = new System.Drawing.Point(145, 247);
            this.btn_elime.Name = "btn_elime";
            this.btn_elime.Size = new System.Drawing.Size(75, 23);
            this.btn_elime.TabIndex = 3;
            this.btn_elime.Text = "Eliminar";
            this.btn_elime.UseVisualStyleBackColor = true;
            this.btn_elime.Click += new System.EventHandler(this.btn_elime_Click);
            // 
            // Administra_Encargados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(554, 409);
            this.Controls.Add(this.tbC_encargado);
            this.Name = "Administra_Encargados";
            this.Text = "Administra_encargados";
            this.tbC_encargado.ResumeLayout(false);
            this.tbP_ae.ResumeLayout(false);
            this.tbP_ae.PerformLayout();
            this.tbP_me.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tbP_ee.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tbC_encargado;
        private System.Windows.Forms.TabPage tbP_ae;
        private System.Windows.Forms.TextBox txt_pass2;
        private System.Windows.Forms.Label lbl_pass2;
        private System.Windows.Forms.TextBox txt_ide;
        private System.Windows.Forms.Label lbl_ide;
        private System.Windows.Forms.Button btnA_salir;
        private System.Windows.Forms.Button btnA_agregar;
        private System.Windows.Forms.ComboBox cBA_nivel;
        private System.Windows.Forms.TextBox tBA_pass;
        private System.Windows.Forms.TextBox tBA_nombre;
        private System.Windows.Forms.Label lblA_nivel;
        private System.Windows.Forms.Label lblA_pass;
        private System.Windows.Forms.Label lblA_nombre;
        private System.Windows.Forms.TabPage tbP_me;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox txtNombreModifica;
        private System.Windows.Forms.Label lblControl;
        private System.Windows.Forms.Button btnModificar;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TabPage tbP_ee;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button bntLimpiar;
        private System.Windows.Forms.Label lblconE;
        private System.Windows.Forms.Label lblNombreE;
        private System.Windows.Forms.TextBox txtControl;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_elime;
    }
}