﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cubiculos
{
    public class PrestamoNodo
    {
        public int personas;
        public int cubiculo;
        public DateTime entrada;
        public char prestamo;
        public int id_alumno;
        public int id_encargado;
        public string carrera;
        public PrestamoNodo sig;

        public PrestamoNodo(int personas, int cubiculo, DateTime entrada, char prestamo, int alumno, int encargado, string carrera)
        {
            this.personas = personas;
            this.cubiculo = cubiculo;
            this.entrada = entrada;
            this.prestamo = prestamo;
            id_alumno = alumno;
            id_encargado = encargado;
            this.carrera = carrera;
        }
    }
}
