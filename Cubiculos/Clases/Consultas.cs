﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cubiculos
{
    class Consultas
    {

        string constring = "Data Source=URIEL;Initial Catalog=Cubiculos_biblioteca;Integrated Security=True";
        SqlConnection conn;
        SqlCommand cmd;
        public static DataSet ds;

        void Conexion()
        {
            conn = new SqlConnection();
            conn.ConnectionString = constring;
            cmd = new SqlCommand();
        }

        public PrestamoNodo insertaNodo(PrestamoNodo nuevo, PrestamoNodo cabeza)
        {
            PrestamoNodo P = cabeza;
            if (cabeza == null)
            {
                cabeza = nuevo;
            }
            else
            {
                while (P.sig != null)
                {
                    P = P.sig;
                }
                P.sig = nuevo;
            }

            return cabeza;
        }

        public PrestamoNodo eliminaNodo(int cubiculo, PrestamoNodo cabeza)
        {
            try
            {
                PrestamoNodo P = cabeza;
                PrestamoNodo Q = P.sig;
                if (cabeza.cubiculo == cubiculo)
                {
                    cabeza = cabeza.sig;
                }
                else
                {
                    while (P.sig != null)
                    {
                        Q = P;
                        P = P.sig;
                        if (P.cubiculo == cubiculo)
                        {
                            Q.sig = P.sig;
                        }
                    }
                }
                return cabeza;
            }
            catch
            {
                return cabeza;
            }
        }

        public PrestamoNodo modificaNodo(int cubiculo, PrestamoNodo cabeza, int alumno)
        {
            try
            {
                PrestamoNodo P = cabeza;
                if (cabeza.cubiculo == cubiculo)
                {
                    cabeza = cabeza.sig;
                }
                else
                {
                    while (P.sig != null)
                    {
                        if (P.cubiculo == cubiculo)
                        {
                            P.id_alumno = alumno;
                        }
                    }
                }
                return cabeza;
            }
            catch
            {
                return cabeza;
            }
        }

        public List<object> LlenarCarreras()
        {
            int[] carreras;
            string[] nombres;

            List<object> lista = new List<object>();

            Conexion();
            string query = "SELECT id_carrera, nombre_carrera from T_CARRERAS";
            cmd.CommandText = query;
            try
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = conn;
                conn.Open();
                SqlDataReader sqlr = cmd.ExecuteReader();
                int contador = 0;

                while (sqlr.Read())
                {
                    contador++;
                }

                conn.Close();

                carreras = new int[contador];
                nombres = new string[contador];

                conn.Open();
                sqlr = cmd.ExecuteReader();

                contador = 0;

                while (sqlr.Read())
                {
                    nombres[contador] = sqlr["nombre_carrera"].ToString();
                    carreras[contador] = (int)sqlr["id_carrera"];
                    contador++;
                }
                sqlr.Close();

                lista.Add(nombres);
                lista.Add(carreras);
                return lista;

            }
            catch(Exception)
            {
                return null;
            }
        }

        public string obtenNombre(string control)
        {
            Conexion();
            string query = "SELECT nombre_alumno from T_ALUMNOS where id_alumno = " + control;
            cmd.CommandText = query;
            try
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = conn;
                conn.Open();
                string nombre = (string)cmd.ExecuteScalar();
                conn.Close();

                query = "SELECT apellido_paterno from T_ALUMNOS where id_alumno = " + control;
                cmd.CommandText = query;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = conn;
                conn.Open();
                nombre += " " + (string)cmd.ExecuteScalar();
                conn.Close();

                query = "SELECT apellido_materno from T_ALUMNOS where id_alumno = " + control;
                cmd.CommandText = query;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = conn;
                conn.Open();
                nombre += " " + (string)cmd.ExecuteScalar();
                conn.Close();

                return nombre;
            }
            catch (Exception)
            {
                return "nell";
            }
            finally
            {
                conn.Close();
            }
        }

        public string obtenCarrera(string control)
        {
            Conexion();
            string query = "SELECT nombre_carrera from V_ALUMNOS where id_alumno = " + control;
            cmd.CommandText = query;
            try
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = conn;
                conn.Open();
                return (string)cmd.ExecuteScalar();
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public string obtenEncargado(string id)
        {
            Conexion();
            string query = "SELECT nombre_encargado from T_ENCARGADOS where id_encargado = " + id;
            cmd.CommandText = query;
            try
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = conn;
                conn.Open();
                return (string)cmd.ExecuteScalar();
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public string obtenPass(int id)
        {
            Conexion();
            string query = "SELECT contraseña from T_ENCARGADOS where id_encargado = " + id;
            cmd.CommandText = query;
            try
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = conn;
                conn.Open();
                return (string)cmd.ExecuteScalar();
            }
            catch(Exception e)
            {
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public int obtenNivel(int id)
        {
            Conexion();
            string query = "SELECT nivel_encargado from T_ENCARGADOS where id_encargado = " + id;
            cmd.CommandText = query;
            try
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = conn;
                conn.Open();
                return (int)cmd.ExecuteScalar();
            }
            catch (Exception)
            {
                return 0;
            }
            finally
            {
                conn.Close();
            }
        }

        public string insertaPrestamoCubiculo(int personas, int cubiculo, DateTime entrada, char prestamo, int alumno, int encargado, string carrera)
        {
            Conexion();
            string fecha = DateTime.Today.Day + "-" + DateTime.Today.Month + "-" + DateTime.Today.Year;
            string ent = entrada.Hour + ":" + entrada.Minute;
            string salida = DateTime.Now.Hour + ":" + DateTime.Now.Minute;

            string query = "INSERT INTO T_CUBICULOS values( @fecha , @personas,@cubiculo,'"+ent+"','"+ salida +"',@prestamo,@alumno,@encargado,@carrera)";
            cmd.CommandText = query;

            

            try
            {
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@fecha", DateTime.Now.Date);
                cmd.Parameters.AddWithValue("@personas", personas);
                cmd.Parameters.AddWithValue("@cubiculo", cubiculo);
                cmd.Parameters.AddWithValue("@prestamo", prestamo);
                cmd.Parameters.AddWithValue("@alumno", alumno);
                cmd.Parameters.AddWithValue("@encargado", encargado);
                cmd.Parameters.AddWithValue("@carrera", carrera);
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
                return "";
            }
            catch (Exception)
            {
                return "No se ha podido completar el registro";
            }
            finally
            {
                conn.Close();
            }
        }

        public string Agregar_Encargado(int id, string nombre, string pass, string nivel)
        {
            Conexion();
            string query = "INSERT INTO T_Encargados (id_encargado, nombre_encargado, contraseña, nivel_encargado) VALUES (@id, @nombre, @pass, @nivel)";
            cmd.CommandText = query;
            try
            {
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@nombre", nombre);
                cmd.Parameters.AddWithValue("@pass", pass);
                cmd.Parameters.AddWithValue("@nivel", nivel);
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
                return ("Encargado agregado.");
            }
            catch (Exception)
            {
                return ("No se ha podido agregar un nuevo encargado.");
            }
            finally
            {
                conn.Close();
            }
        }

        public string Agregar_Alumno(int id, string nombre, int carrera)
        {
            Conexion();
            string query = "INSERT INTO T_ALUMNOS VALUES (@id, @nombre, @carrera)";
            cmd.CommandText = query;
            try
            {
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@nombre", nombre);
                cmd.Parameters.AddWithValue("@carrera", carrera);
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
                return ("Alumno agregado.");
            }
            catch (Exception e)
            {
                return ("No se ha podido agregar un nuevo alumno. \n Revise si el alumno ya existe en el sistema.");
            }
            finally
            {
                conn.Close();
            }
        }

        public string Modificar_Alumno(int id, string nombre, int carrera)
        {
            Conexion();
            string query = "UPDATE T_ALUMNOS SET nombre_alumno = @nombre, carrera_alumno = @carrera WHERE id_alumno = @id";
            cmd.CommandText = query;
            try
            {
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@nombre", nombre);
                cmd.Parameters.AddWithValue("@carrera", carrera);
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
                return ("Alumno modificado.");
            }
            catch (Exception e)
            {
                return ("No se ha podido modificar un nuevo alumno.");
            }
            finally
            {
                conn.Close();
            }
        }

        public string Eliminar_Alumno(int id, string nombre)
        {
            Conexion();
            string query = "DELETE T_ALUMNOS WHERE id_alumno = " + id;
            cmd.CommandText = query;
            try
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
                return ("Alumno eliminado.");
            }
            catch (Exception e)
            {
                return ("No se ha podido eliminar al alumno: " + nombre);
            }
            finally
            {
                conn.Close();
            }
        }

        public string Enviar_Sugerencia(string sugerencia)
        {
            Conexion();
            string query = "INSERT INTO T_SUGERENCIAS VALUES (@sugerencia)";
            cmd.CommandText = query;
            try
            {
                cmd.Parameters.AddWithValue("@sugerencia", sugerencia);
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
                return ("Gracias por ayudarnos a mejorar :)");
            }
            catch (Exception e)
            {
                return ("Gracias por ayudarnos a mejorar :(");
            }
            finally
            {
                conn.Close();
            }
        }

        public string Eliminar_Encargado(int id)
        {
            Conexion();
            string query = "DELETE T_ENCARGADOS WHERE id_encargado = @id";
            cmd.CommandText = query;
            try
            {
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
                return ("Encargado eliminado.");
            }
            catch (Exception e)
            {
                return ("No se ha podido eliminar al encargado.");
            }
            finally
            {
                conn.Close();
            }
        }

        public string Modificar_Encargado(int id, string nombre, string pass)
        {
            Conexion();
            string query = "UPDATE T_ENCARGADOS SET nombre_encargado = @nombre, contraseña = @pass WHERE id_encargado = @id";
            cmd.CommandText = query;
            try
            {
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@nombre", nombre);
                cmd.Parameters.AddWithValue("@pass", pass);
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
                return ("Encargado modificado.");
            }
            catch (Exception e)
            {
                return ("No se ha podido modificar el encargado.");
            }
            finally
            {
                conn.Close();
            }
        }

        public string Modificar_EncargadoN(int id, string nombre)
        {
            Conexion();
            string query = "UPDATE T_ENCARGADOS SET nombre_encargado = @nombre WHERE id_encargado = @id";
            cmd.CommandText = query;
            try
            {
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@nombre", nombre);
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
                return ("Encargado modificado.");
            }
            catch (Exception e)
            {
                return ("No se ha podido modificar el encargado.");
            }
            finally
            {
                conn.Close();
            }
        }

        public List<object> DataGrid(string fechaI, string fechaF)
        {
            int[] id_carreras;
            string[] carreras;
            int[] alumnos;

            List<object> lista = new List<object>();

            Conexion();
            string query = "SELECT id_carrera, carrera_bitacora, SUM(num_personas_b) AS Alumnos FROM T_BITACORA JOIN T_CARRERAS ON nombre_carrera = carrera_bitacora WHERE fecha_bitacora >= @fechaI AND fecha_bitacora <= @fechaF GROUP BY carrera_bitacora, id_carrera";
            cmd.CommandText = query;
            try
            {
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@fechaI", fechaI);
                cmd.Parameters.AddWithValue("@fechaF", fechaF);
                cmd.Connection = conn;
                conn.Open();
                SqlDataReader sqlr = cmd.ExecuteReader();
                int contador = 0;

                while (sqlr.Read())
                {
                    contador++;
                }

                conn.Close();

                id_carreras = new int[contador];
                carreras = new string[contador];
                alumnos = new int[contador];

                conn.Open();
                sqlr = cmd.ExecuteReader();

                contador = 0;

                while (sqlr.Read())
                {
                    id_carreras[contador] = (int)sqlr["id_carrera"];
                    carreras[contador] = sqlr["carrera_bitacora"].ToString();
                    alumnos[contador] = (int)sqlr["Alumnos"];
                    contador++;
                }
                sqlr.Close();

                lista.Add(id_carreras);
                lista.Add(carreras);
                lista.Add(alumnos);
                return lista;

            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
