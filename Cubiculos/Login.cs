﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cubiculos
{
    public partial class Login : Form
    {
        Consultas cons;
        Form1 principal;
        public Login()
        {
            InitializeComponent();
            cons = new Consultas();
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            string pass;
            int ID;

            try
            {
                ID = Convert.ToInt32(txtID.Text);
                pass = cons.obtenPass(ID);
                string encargado = cons.obtenEncargado(txtID.Text);

                if (pass.Equals(txtPass.Text))
                {
                    MessageBox.Show("Bienvenido " + encargado + ".");

                    if (principal == null)
                    {
                        principal = new Form1(this);
                    }
                    principal.id_encargado = Convert.ToInt32(txtID.Text);
                    principal.lblEncargado.Text = "Encargado Actual: " + cons.obtenEncargado(txtID.Text);
                    this.Visible = false;
                    principal.Visible = true;
                    if(cons.obtenNivel(principal.id_encargado) == 1)
                    {
                        principal.tmiAdministrador.Visible = true;
                    }
                    else
                    {
                        principal.tmiAdministrador.Visible = false;
                    }
                }
                else
                {
                    MessageBox.Show("El Usuario y la Contraseña no coinciden.");
                }
            }
            catch
            {
                MessageBox.Show("Revise los datos.");
            }
            finally
            {
                txtID.Focus();
                txtID.Clear();
                txtPass.Clear();
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            if (principal != null)
            {
                DialogResult boton = MessageBox.Show("Si sale no se registraran los prestamos que estén activos actualmente. \n ¿Desea Continuar?", "Alerta", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (boton == DialogResult.OK)
                {
                    for (int i = 1; i < 9; i++)
                    {
                        principal.cancelaPrestamo(i);
                    }
                    Application.Exit();
                }
            }
            else
                Application.Exit();
        }

        private void txtID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnEntrar_Click(this, new EventArgs());
            }
        }

        private void txtID_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtPass.Focus();
            }
        }
    }
}
