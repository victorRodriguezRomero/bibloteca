﻿using iTextSharp.text.pdf;
using iTextSharp.text;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace Cubiculos
{
    public partial class Reportes : Form
    {
        Consultas cons;
        Form1 principal;
        int[] alumnos;
        int[] id_carrera;
        string[,] m_grid = new string[3, 15];
        int total;

        public Reportes(Form1 principal)
        {
            InitializeComponent();
            cons = new Consultas();
            this.principal = principal;
            total = 0;
            this.dtp_intervaloI.Value = DateTime.Today;
            this.dtp_intervaloF.Value = DateTime.Today;
            //llenamatriz();
            //llenaalumnos();
            Llenagrid();
        }

        void Llenagrid()
        {
            dGV_reportes.Rows.Clear();
            llenaalumnos();
            string fechaIn = dtp_intervaloI.Value.Year.ToString() + "-" + dtp_intervaloI.Value.Month.ToString() + "-" + dtp_intervaloI.Value.Day.ToString();
            string fechaFi = dtp_intervaloF.Value.Year.ToString() + "-" + dtp_intervaloF.Value.Month.ToString() + "-" + dtp_intervaloF.Value.Day.ToString();
            try
            {
                for (int i = 0; i < 15; i++)
                {
                    dGV_reportes.Rows.Add(m_grid[0, i], m_grid[1, i], m_grid[2, i]);
                }
            }

            catch (Exception e)
            { }
            total_alumnos();
        }

        private void btn_printR_Click_1(object sender, EventArgs e)
        {
            iTextSharp.text.Font fuente = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 16);
            try
            {
                iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(PageSize.A2, 10f, 10f, 10f, 0f);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, new FileStream("C:/RegistrosCubiculos/REPORTES/" + "Rep " + dtp_intervaloI.Value.Year.ToString() + "-" + dtp_intervaloI.Value.Month.ToString() + "-" + dtp_intervaloI.Value.Day.ToString() + " al " + dtp_intervaloF.Value.Year.ToString() + "-" + dtp_intervaloF.Value.Month.ToString() + "-" + dtp_intervaloF.Value.Day.ToString() + ".pdf", FileMode.Create));
                pdfDoc.Open();

                iTextSharp.text.Image jpg1 = iTextSharp.text.Image.GetInstance("C:/RegistrosCubiculos/encabezado.JPG");
                jpg1.Alignment = Element.ALIGN_CENTER;
                pdfDoc.Add(jpg1);


                PdfContentByte cb = writer.DirectContent;
                PdfPTable pdfTable = new PdfPTable(dGV_reportes.ColumnCount);
                pdfTable.DefaultCell.Padding = 5;
                pdfTable.WidthPercentage = 50;
                pdfTable.TotalWidth = 960;
                pdfTable.DefaultCell.BorderWidth = 1;
                foreach (DataGridViewColumn column in dGV_reportes.Columns)
                {
                    PdfPCell cell = new PdfPCell(new Phrase(column.HeaderText, fuente));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    pdfTable.AddCell(cell);
                }
                foreach (DataGridViewRow row in dGV_reportes.Rows)
                {
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        PdfPCell cel = new PdfPCell(new Phrase(cell.Value.ToString(), fuente));
                        cel.HorizontalAlignment = Element.ALIGN_CENTER;
                        pdfTable.AddCell(cel);
                    }
                }


                pdfTable.WriteSelectedRows(0, -1, 130, 1302, cb);
                BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb.SetColorFill(BaseColor.BLACK);
                cb.SetFontAndSize(bf, 24);

                cb.BeginText();
                string text = "“Año del Centenario de la Promulgación de la Constitución Política de los Estados Unidos Mexicanos”";
                cb.ShowTextAligned(2, text, 1100, 1470, 0);
                cb.EndText();
                cb.BeginText();
                text = "REPORTE DE CUBICULOS     " + dtp_intervaloI.Value.ToShortDateString() + "  al  " + dtp_intervaloF.Value.ToShortDateString();
                cb.ShowTextAligned(4, text, 340, 1330, 0);
                cb.EndText();
                cb.BeginText();
                text = "TOTAL DE ALUMNOS: " + total;
                cb.ShowTextAligned(3, text, 666, 800, 0);
                cb.EndText();

                iTextSharp.text.Image jpg2 = iTextSharp.text.Image.GetInstance("C:/RegistrosCubiculos/piedepagina.JPG");
                jpg2.SetAbsolutePosition(200, 0);
                pdfDoc.Add(jpg2);
                pdfDoc.Close();
                MessageBox.Show("Reporte generado.", "Exito", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            { MessageBox.Show("Error con el documento."); }
        }

        private void dtp_intervaloI_ValueChanged_1(object sender, EventArgs e)
        {
            Llenagrid();
        }

        private void dtp_intervaloF_ValueChanged_1(object sender, EventArgs e)
        {
            Llenagrid();
        }

        public void llenamatriz()
        {
            m_grid = new string[,] { { "34", "33", "35", "6", "11", "32", "10", "8", "5", "31", "16", "24", "28", "27", "29" }, { "ING. BIOMEDICA", "ING. EN INFORMATICA", "ING. IND. 100 INGLES", "INGENIERIA ELECTRICA", "INGENIERIA ELECTRONICA", "INGENIERIA EN GESTION EMPRESARIAL", "INGENIERIA EN SISTEMAS COMPUTACIONALES", "INGENIERIA INDUSTRIAL", "INGENIERIA MECANICA", "INGENIERIA MECATRONICA", "LICENCIATURA EN ADMINISTRACION", "MAESTRIA EN ADMINISTRACION", "MAESTRIA EN CIENCIAS DE LA COMPUTACION", "MAESTRIA EN INGENIERIA ELECTRONICA", "MAESTRIA EN INGENIERIA INDUSTRIAL" }, { "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0" } };
        }

        public void llenaalumnos()
        {
            llenamatriz();
            string fechaIn = dtp_intervaloI.Value.Year.ToString() + "-" + dtp_intervaloI.Value.Month.ToString() + "-" + dtp_intervaloI.Value.Day.ToString();
            string fechaFi = dtp_intervaloF.Value.Year.ToString() + "-" + dtp_intervaloF.Value.Month.ToString() + "-" + dtp_intervaloF.Value.Day.ToString();
            List<object> lista = cons.DataGrid(fechaIn, fechaFi);

            id_carrera = (int[])lista[0];
            alumnos = (int[])lista[2];
            try
            {
                for (int x = 0; x < id_carrera.Length; x++)
                {
                    for (int y = 0; y < 15; y++)
                    {
                        if (id_carrera[x].ToString() == m_grid[0, y])
                        {
                            m_grid[2, y] = alumnos[x].ToString();
                            break;
                        }
                    }
                }
            }
            catch (Exception)
            { }
        }

        public void total_alumnos()
        {
            total = 0;
            try
            {
                for (int i = 0; i < dGV_reportes.Rows.Count; i++)
                {
                    total = total + Convert.ToInt32(dGV_reportes.Rows[i].Cells[2].Value);
                }
            }
            catch (Exception)
            { }
        }

        
    }
}
