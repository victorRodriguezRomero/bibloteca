﻿namespace Cubiculos
{
    partial class Prestamo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPersonas = new System.Windows.Forms.Label();
            this.ckbPrestamo = new System.Windows.Forms.CheckBox();
            this.nudPersonas = new System.Windows.Forms.NumericUpDown();
            this.btnOk = new System.Windows.Forms.Button();
            this.lblHora = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nudPersonas)).BeginInit();
            this.SuspendLayout();
            // 
            // lblPersonas
            // 
            this.lblPersonas.AutoSize = true;
            this.lblPersonas.Location = new System.Drawing.Point(12, 9);
            this.lblPersonas.Name = "lblPersonas";
            this.lblPersonas.Size = new System.Drawing.Size(54, 13);
            this.lblPersonas.TabIndex = 29;
            this.lblPersonas.Text = "Personas:";
            // 
            // ckbPrestamo
            // 
            this.ckbPrestamo.AutoSize = true;
            this.ckbPrestamo.Location = new System.Drawing.Point(12, 42);
            this.ckbPrestamo.Name = "ckbPrestamo";
            this.ckbPrestamo.Size = new System.Drawing.Size(144, 17);
            this.ckbPrestamo.TabIndex = 1;
            this.ckbPrestamo.Text = "Prestamo de Marcadores";
            this.ckbPrestamo.UseVisualStyleBackColor = true;
            // 
            // nudPersonas
            // 
            this.nudPersonas.Location = new System.Drawing.Point(101, 7);
            this.nudPersonas.Name = "nudPersonas";
            this.nudPersonas.ReadOnly = true;
            this.nudPersonas.Size = new System.Drawing.Size(34, 20);
            this.nudPersonas.TabIndex = 0;
            this.nudPersonas.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.nudPersonas.ValueChanged += new System.EventHandler(this.nudPersonas_ValueChanged);
            this.nudPersonas.KeyDown += new System.Windows.Forms.KeyEventHandler(this.nudPersonas_KeyDown);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(15, 76);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "Siguiente";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // lblHora
            // 
            this.lblHora.AutoSize = true;
            this.lblHora.Location = new System.Drawing.Point(121, 81);
            this.lblHora.Name = "lblHora";
            this.lblHora.Size = new System.Drawing.Size(30, 13);
            this.lblHora.TabIndex = 32;
            this.lblHora.Text = "Hora";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(15, 107);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 3;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // Prestamo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(302, 142);
            this.ControlBox = false;
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.lblHora);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.lblPersonas);
            this.Controls.Add(this.ckbPrestamo);
            this.Controls.Add(this.nudPersonas);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Prestamo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Prestamo";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.nudPersonas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPersonas;
        private System.Windows.Forms.CheckBox ckbPrestamo;
        private System.Windows.Forms.NumericUpDown nudPersonas;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Label lblHora;
        private System.Windows.Forms.Button btnCancelar;
    }
}