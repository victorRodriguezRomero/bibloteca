﻿namespace Cubiculos
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblControl = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.lblCarrera = new System.Windows.Forms.Label();
            this.txtControl = new System.Windows.Forms.TextBox();
            this.btnSiguiente = new System.Windows.Forms.Button();
            this.lblCarreraAlumno = new System.Windows.Forms.Label();
            this.lblNombreAlumno = new System.Windows.Forms.Label();
            this.btnCubiculo1 = new System.Windows.Forms.Button();
            this.btnCubiculo2 = new System.Windows.Forms.Button();
            this.btnCubiculo3 = new System.Windows.Forms.Button();
            this.btnCubiculo4 = new System.Windows.Forms.Button();
            this.btnCubiculo5 = new System.Windows.Forms.Button();
            this.btnCubiculo6 = new System.Windows.Forms.Button();
            this.btnCubiculo7 = new System.Windows.Forms.Button();
            this.btnCubiculo8 = new System.Windows.Forms.Button();
            this.msMenu = new System.Windows.Forms.MenuStrip();
            this.opcionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tmiAdministrador = new System.Windows.Forms.ToolStripMenuItem();
            this.tmiEncargados = new System.Windows.Forms.ToolStripMenuItem();
            this.tmiAlumnos = new System.Windows.Forms.ToolStripMenuItem();
            this.tmiReportes = new System.Windows.Forms.ToolStripMenuItem();
            this.btnCancel1 = new System.Windows.Forms.Button();
            this.btnCancel2 = new System.Windows.Forms.Button();
            this.btnCancel3 = new System.Windows.Forms.Button();
            this.btnCancel4 = new System.Windows.Forms.Button();
            this.btnCancel5 = new System.Windows.Forms.Button();
            this.btnCancel6 = new System.Windows.Forms.Button();
            this.btnCancel7 = new System.Windows.Forms.Button();
            this.btnCancel8 = new System.Windows.Forms.Button();
            this.lblHora1 = new System.Windows.Forms.Label();
            this.lblHora2 = new System.Windows.Forms.Label();
            this.lblHora3 = new System.Windows.Forms.Label();
            this.lblHora4 = new System.Windows.Forms.Label();
            this.lblHora5 = new System.Windows.Forms.Label();
            this.lblHora6 = new System.Windows.Forms.Label();
            this.lblHora7 = new System.Windows.Forms.Label();
            this.lblHora8 = new System.Windows.Forms.Label();
            this.lblEncargado = new System.Windows.Forms.Label();
            this.txtSugerencia = new System.Windows.Forms.TextBox();
            this.btnEnviarSugerencia = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_cancelar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblEncargadoCub1 = new System.Windows.Forms.Label();
            this.lblEncargadoCub2 = new System.Windows.Forms.Label();
            this.lblEncargadoCub3 = new System.Windows.Forms.Label();
            this.lblEncargadoCub4 = new System.Windows.Forms.Label();
            this.lblEncargadoCub5 = new System.Windows.Forms.Label();
            this.lblEncargadoCub6 = new System.Windows.Forms.Label();
            this.lblEncargadoCub7 = new System.Windows.Forms.Label();
            this.lblEncargadoCub8 = new System.Windows.Forms.Label();
            this.btnEditar1 = new System.Windows.Forms.Button();
            this.btnEditar2 = new System.Windows.Forms.Button();
            this.btnEditar3 = new System.Windows.Forms.Button();
            this.btnEditar4 = new System.Windows.Forms.Button();
            this.btnEditar5 = new System.Windows.Forms.Button();
            this.btnEditar6 = new System.Windows.Forms.Button();
            this.btnEditar7 = new System.Windows.Forms.Button();
            this.btnEditar8 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.msMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblControl
            // 
            this.lblControl.AutoSize = true;
            this.lblControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblControl.Location = new System.Drawing.Point(25, 94);
            this.lblControl.Name = "lblControl";
            this.lblControl.Size = new System.Drawing.Size(71, 13);
            this.lblControl.TabIndex = 0;
            this.lblControl.Text = "No. Control";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.Location = new System.Drawing.Point(418, 94);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(54, 13);
            this.lblNombre.TabIndex = 1;
            this.lblNombre.Text = "Nombre:";
            // 
            // lblCarrera
            // 
            this.lblCarrera.AutoSize = true;
            this.lblCarrera.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCarrera.Location = new System.Drawing.Point(748, 94);
            this.lblCarrera.Name = "lblCarrera";
            this.lblCarrera.Size = new System.Drawing.Size(52, 13);
            this.lblCarrera.TabIndex = 2;
            this.lblCarrera.Text = "Carrera:";
            // 
            // txtControl
            // 
            this.txtControl.BackColor = System.Drawing.SystemColors.Window;
            this.txtControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtControl.Location = new System.Drawing.Point(115, 92);
            this.txtControl.MaxLength = 8;
            this.txtControl.Name = "txtControl";
            this.txtControl.Size = new System.Drawing.Size(106, 20);
            this.txtControl.TabIndex = 0;
            this.txtControl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtControl_KeyDown);
            this.txtControl.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtControl_KeyPress);
            // 
            // btnSiguiente
            // 
            this.btnSiguiente.Location = new System.Drawing.Point(243, 89);
            this.btnSiguiente.Name = "btnSiguiente";
            this.btnSiguiente.Size = new System.Drawing.Size(75, 23);
            this.btnSiguiente.TabIndex = 1;
            this.btnSiguiente.Text = "Siguiente";
            this.btnSiguiente.UseVisualStyleBackColor = true;
            this.btnSiguiente.Click += new System.EventHandler(this.btnSiguiente_Click);
            // 
            // lblCarreraAlumno
            // 
            this.lblCarreraAlumno.AutoSize = true;
            this.lblCarreraAlumno.Location = new System.Drawing.Point(799, 94);
            this.lblCarreraAlumno.Name = "lblCarreraAlumno";
            this.lblCarreraAlumno.Size = new System.Drawing.Size(0, 13);
            this.lblCarreraAlumno.TabIndex = 8;
            // 
            // lblNombreAlumno
            // 
            this.lblNombreAlumno.AutoSize = true;
            this.lblNombreAlumno.Location = new System.Drawing.Point(478, 94);
            this.lblNombreAlumno.Name = "lblNombreAlumno";
            this.lblNombreAlumno.Size = new System.Drawing.Size(0, 13);
            this.lblNombreAlumno.TabIndex = 7;
            // 
            // btnCubiculo1
            // 
            this.btnCubiculo1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCubiculo1.BackColor = System.Drawing.Color.YellowGreen;
            this.btnCubiculo1.Location = new System.Drawing.Point(110, 128);
            this.btnCubiculo1.Name = "btnCubiculo1";
            this.btnCubiculo1.Size = new System.Drawing.Size(169, 138);
            this.btnCubiculo1.TabIndex = 2;
            this.btnCubiculo1.Text = "Cubiculo 1";
            this.btnCubiculo1.UseVisualStyleBackColor = false;
            this.btnCubiculo1.Click += new System.EventHandler(this.IniciaTimer);
            // 
            // btnCubiculo2
            // 
            this.btnCubiculo2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCubiculo2.BackColor = System.Drawing.Color.YellowGreen;
            this.btnCubiculo2.Location = new System.Drawing.Point(412, 128);
            this.btnCubiculo2.Name = "btnCubiculo2";
            this.btnCubiculo2.Size = new System.Drawing.Size(169, 138);
            this.btnCubiculo2.TabIndex = 4;
            this.btnCubiculo2.Text = "Cubiculo 2";
            this.btnCubiculo2.UseVisualStyleBackColor = false;
            this.btnCubiculo2.Click += new System.EventHandler(this.IniciaTimer);
            // 
            // btnCubiculo3
            // 
            this.btnCubiculo3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCubiculo3.BackColor = System.Drawing.Color.YellowGreen;
            this.btnCubiculo3.Location = new System.Drawing.Point(712, 128);
            this.btnCubiculo3.Name = "btnCubiculo3";
            this.btnCubiculo3.Size = new System.Drawing.Size(169, 138);
            this.btnCubiculo3.TabIndex = 6;
            this.btnCubiculo3.Text = "Cubiculo 3";
            this.btnCubiculo3.UseVisualStyleBackColor = false;
            this.btnCubiculo3.Click += new System.EventHandler(this.IniciaTimer);
            // 
            // btnCubiculo4
            // 
            this.btnCubiculo4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCubiculo4.BackColor = System.Drawing.Color.YellowGreen;
            this.btnCubiculo4.Location = new System.Drawing.Point(1012, 128);
            this.btnCubiculo4.Name = "btnCubiculo4";
            this.btnCubiculo4.Size = new System.Drawing.Size(169, 138);
            this.btnCubiculo4.TabIndex = 8;
            this.btnCubiculo4.Text = "Cubiculo 4";
            this.btnCubiculo4.UseVisualStyleBackColor = false;
            this.btnCubiculo4.Click += new System.EventHandler(this.IniciaTimer);
            // 
            // btnCubiculo5
            // 
            this.btnCubiculo5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCubiculo5.BackColor = System.Drawing.Color.Brown;
            this.btnCubiculo5.Location = new System.Drawing.Point(110, 355);
            this.btnCubiculo5.Name = "btnCubiculo5";
            this.btnCubiculo5.Size = new System.Drawing.Size(169, 138);
            this.btnCubiculo5.TabIndex = 10;
            this.btnCubiculo5.Text = "Cubiculo 5";
            this.btnCubiculo5.UseVisualStyleBackColor = false;
            this.btnCubiculo5.Click += new System.EventHandler(this.IniciaTimer);
            // 
            // btnCubiculo6
            // 
            this.btnCubiculo6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCubiculo6.BackColor = System.Drawing.Color.YellowGreen;
            this.btnCubiculo6.Location = new System.Drawing.Point(412, 355);
            this.btnCubiculo6.Name = "btnCubiculo6";
            this.btnCubiculo6.Size = new System.Drawing.Size(169, 138);
            this.btnCubiculo6.TabIndex = 12;
            this.btnCubiculo6.Text = "Cubiculo 6";
            this.btnCubiculo6.UseVisualStyleBackColor = false;
            this.btnCubiculo6.Click += new System.EventHandler(this.IniciaTimer);
            // 
            // btnCubiculo7
            // 
            this.btnCubiculo7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCubiculo7.BackColor = System.Drawing.Color.YellowGreen;
            this.btnCubiculo7.Location = new System.Drawing.Point(712, 355);
            this.btnCubiculo7.Name = "btnCubiculo7";
            this.btnCubiculo7.Size = new System.Drawing.Size(169, 138);
            this.btnCubiculo7.TabIndex = 14;
            this.btnCubiculo7.Text = "Cubiculo 7";
            this.btnCubiculo7.UseVisualStyleBackColor = false;
            this.btnCubiculo7.Click += new System.EventHandler(this.IniciaTimer);
            // 
            // btnCubiculo8
            // 
            this.btnCubiculo8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCubiculo8.BackColor = System.Drawing.Color.YellowGreen;
            this.btnCubiculo8.Location = new System.Drawing.Point(1012, 355);
            this.btnCubiculo8.Name = "btnCubiculo8";
            this.btnCubiculo8.Size = new System.Drawing.Size(169, 138);
            this.btnCubiculo8.TabIndex = 16;
            this.btnCubiculo8.Text = "INEGI";
            this.btnCubiculo8.UseVisualStyleBackColor = false;
            this.btnCubiculo8.Click += new System.EventHandler(this.IniciaTimer);
            // 
            // msMenu
            // 
            this.msMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.opcionesToolStripMenuItem,
            this.tmiAdministrador});
            this.msMenu.Location = new System.Drawing.Point(0, 0);
            this.msMenu.Name = "msMenu";
            this.msMenu.Size = new System.Drawing.Size(1301, 24);
            this.msMenu.TabIndex = 17;
            this.msMenu.Text = "menuStrip1";
            // 
            // opcionesToolStripMenuItem
            // 
            this.opcionesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salirToolStripMenuItem});
            this.opcionesToolStripMenuItem.Name = "opcionesToolStripMenuItem";
            this.opcionesToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.opcionesToolStripMenuItem.Text = "Opciones";
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(99, 22);
            this.salirToolStripMenuItem.Text = "Salir.";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // tmiAdministrador
            // 
            this.tmiAdministrador.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tmiEncargados,
            this.tmiAlumnos,
            this.tmiReportes});
            this.tmiAdministrador.Name = "tmiAdministrador";
            this.tmiAdministrador.Size = new System.Drawing.Size(95, 20);
            this.tmiAdministrador.Text = "Administrador";
            // 
            // tmiEncargados
            // 
            this.tmiEncargados.Name = "tmiEncargados";
            this.tmiEncargados.Size = new System.Drawing.Size(200, 22);
            this.tmiEncargados.Text = "Administrar Encargados";
            this.tmiEncargados.Click += new System.EventHandler(this.tmiEncargados_Click);
            // 
            // tmiAlumnos
            // 
            this.tmiAlumnos.Name = "tmiAlumnos";
            this.tmiAlumnos.Size = new System.Drawing.Size(200, 22);
            this.tmiAlumnos.Text = "Administrar Alumnos";
            this.tmiAlumnos.Click += new System.EventHandler(this.tmiAlumnos_Click);
            // 
            // tmiReportes
            // 
            this.tmiReportes.Name = "tmiReportes";
            this.tmiReportes.Size = new System.Drawing.Size(200, 22);
            this.tmiReportes.Text = "Reportes";
            this.tmiReportes.Click += new System.EventHandler(this.tmiReportes_Click_1);
            // 
            // btnCancel1
            // 
            this.btnCancel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel1.Location = new System.Drawing.Point(200, 320);
            this.btnCancel1.Name = "btnCancel1";
            this.btnCancel1.Size = new System.Drawing.Size(92, 29);
            this.btnCancel1.TabIndex = 3;
            this.btnCancel1.Text = "Terminar";
            this.btnCancel1.UseVisualStyleBackColor = true;
            this.btnCancel1.Click += new System.EventHandler(this.cancelarHilos);
            // 
            // btnCancel2
            // 
            this.btnCancel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel2.Location = new System.Drawing.Point(500, 320);
            this.btnCancel2.Name = "btnCancel2";
            this.btnCancel2.Size = new System.Drawing.Size(92, 29);
            this.btnCancel2.TabIndex = 5;
            this.btnCancel2.Text = "Terminar";
            this.btnCancel2.UseVisualStyleBackColor = true;
            this.btnCancel2.Click += new System.EventHandler(this.cancelarHilos);
            // 
            // btnCancel3
            // 
            this.btnCancel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel3.Location = new System.Drawing.Point(800, 320);
            this.btnCancel3.Name = "btnCancel3";
            this.btnCancel3.Size = new System.Drawing.Size(92, 29);
            this.btnCancel3.TabIndex = 7;
            this.btnCancel3.Text = "Terminar";
            this.btnCancel3.UseVisualStyleBackColor = true;
            this.btnCancel3.Click += new System.EventHandler(this.cancelarHilos);
            // 
            // btnCancel4
            // 
            this.btnCancel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel4.Location = new System.Drawing.Point(1100, 320);
            this.btnCancel4.Name = "btnCancel4";
            this.btnCancel4.Size = new System.Drawing.Size(92, 29);
            this.btnCancel4.TabIndex = 9;
            this.btnCancel4.Text = "Terminar";
            this.btnCancel4.UseVisualStyleBackColor = true;
            this.btnCancel4.Click += new System.EventHandler(this.cancelarHilos);
            // 
            // btnCancel5
            // 
            this.btnCancel5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel5.Location = new System.Drawing.Point(200, 544);
            this.btnCancel5.Name = "btnCancel5";
            this.btnCancel5.Size = new System.Drawing.Size(92, 29);
            this.btnCancel5.TabIndex = 11;
            this.btnCancel5.Text = "Terminar";
            this.btnCancel5.UseVisualStyleBackColor = true;
            this.btnCancel5.Click += new System.EventHandler(this.cancelarHilos);
            // 
            // btnCancel6
            // 
            this.btnCancel6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel6.Location = new System.Drawing.Point(500, 544);
            this.btnCancel6.Name = "btnCancel6";
            this.btnCancel6.Size = new System.Drawing.Size(92, 29);
            this.btnCancel6.TabIndex = 13;
            this.btnCancel6.Text = "Terminar";
            this.btnCancel6.UseVisualStyleBackColor = true;
            this.btnCancel6.Click += new System.EventHandler(this.cancelarHilos);
            // 
            // btnCancel7
            // 
            this.btnCancel7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel7.Location = new System.Drawing.Point(800, 544);
            this.btnCancel7.Name = "btnCancel7";
            this.btnCancel7.Size = new System.Drawing.Size(92, 29);
            this.btnCancel7.TabIndex = 15;
            this.btnCancel7.Text = "Terminar";
            this.btnCancel7.UseVisualStyleBackColor = true;
            this.btnCancel7.Click += new System.EventHandler(this.cancelarHilos);
            // 
            // btnCancel8
            // 
            this.btnCancel8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel8.Location = new System.Drawing.Point(1100, 544);
            this.btnCancel8.Name = "btnCancel8";
            this.btnCancel8.Size = new System.Drawing.Size(92, 29);
            this.btnCancel8.TabIndex = 17;
            this.btnCancel8.Text = "Terminar";
            this.btnCancel8.UseVisualStyleBackColor = true;
            this.btnCancel8.Click += new System.EventHandler(this.cancelarHilos);
            // 
            // lblHora1
            // 
            this.lblHora1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHora1.AutoSize = true;
            this.lblHora1.Location = new System.Drawing.Point(110, 269);
            this.lblHora1.Name = "lblHora1";
            this.lblHora1.Size = new System.Drawing.Size(66, 13);
            this.lblHora1.TabIndex = 25;
            this.lblHora1.Text = "Hora Inicial: ";
            // 
            // lblHora2
            // 
            this.lblHora2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHora2.AutoSize = true;
            this.lblHora2.Location = new System.Drawing.Point(412, 269);
            this.lblHora2.Name = "lblHora2";
            this.lblHora2.Size = new System.Drawing.Size(69, 13);
            this.lblHora2.TabIndex = 26;
            this.lblHora2.Text = "Hora Inicial:  ";
            // 
            // lblHora3
            // 
            this.lblHora3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHora3.AutoSize = true;
            this.lblHora3.Location = new System.Drawing.Point(712, 269);
            this.lblHora3.Name = "lblHora3";
            this.lblHora3.Size = new System.Drawing.Size(69, 13);
            this.lblHora3.TabIndex = 28;
            this.lblHora3.Text = "Hora Inicial:  ";
            // 
            // lblHora4
            // 
            this.lblHora4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHora4.AutoSize = true;
            this.lblHora4.Location = new System.Drawing.Point(1012, 269);
            this.lblHora4.Name = "lblHora4";
            this.lblHora4.Size = new System.Drawing.Size(69, 13);
            this.lblHora4.TabIndex = 29;
            this.lblHora4.Text = "Hora Inicial:  ";
            // 
            // lblHora5
            // 
            this.lblHora5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHora5.AutoSize = true;
            this.lblHora5.Location = new System.Drawing.Point(110, 496);
            this.lblHora5.Name = "lblHora5";
            this.lblHora5.Size = new System.Drawing.Size(69, 13);
            this.lblHora5.TabIndex = 30;
            this.lblHora5.Text = "Hora Inicial:  ";
            // 
            // lblHora6
            // 
            this.lblHora6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHora6.AutoSize = true;
            this.lblHora6.Location = new System.Drawing.Point(412, 496);
            this.lblHora6.Name = "lblHora6";
            this.lblHora6.Size = new System.Drawing.Size(69, 13);
            this.lblHora6.TabIndex = 31;
            this.lblHora6.Text = "Hora Inicial:  ";
            // 
            // lblHora7
            // 
            this.lblHora7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHora7.AutoSize = true;
            this.lblHora7.Location = new System.Drawing.Point(712, 496);
            this.lblHora7.Name = "lblHora7";
            this.lblHora7.Size = new System.Drawing.Size(69, 13);
            this.lblHora7.TabIndex = 32;
            this.lblHora7.Text = "Hora Inicial:  ";
            // 
            // lblHora8
            // 
            this.lblHora8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHora8.AutoSize = true;
            this.lblHora8.Location = new System.Drawing.Point(1012, 496);
            this.lblHora8.Name = "lblHora8";
            this.lblHora8.Size = new System.Drawing.Size(69, 13);
            this.lblHora8.TabIndex = 33;
            this.lblHora8.Text = "Hora Inicial:  ";
            // 
            // lblEncargado
            // 
            this.lblEncargado.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblEncargado.AutoSize = true;
            this.lblEncargado.Location = new System.Drawing.Point(12, 716);
            this.lblEncargado.Name = "lblEncargado";
            this.lblEncargado.Size = new System.Drawing.Size(98, 13);
            this.lblEncargado.TabIndex = 34;
            this.lblEncargado.Text = "Encargado Actual: ";
            // 
            // txtSugerencia
            // 
            this.txtSugerencia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSugerencia.Location = new System.Drawing.Point(12, 631);
            this.txtSugerencia.Multiline = true;
            this.txtSugerencia.Name = "txtSugerencia";
            this.txtSugerencia.Size = new System.Drawing.Size(1277, 82);
            this.txtSugerencia.TabIndex = 18;
            this.txtSugerencia.TextChanged += new System.EventHandler(this.txtSugerencia_TextChanged);
            // 
            // btnEnviarSugerencia
            // 
            this.btnEnviarSugerencia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEnviarSugerencia.Enabled = false;
            this.btnEnviarSugerencia.Location = new System.Drawing.Point(1227, 711);
            this.btnEnviarSugerencia.Name = "btnEnviarSugerencia";
            this.btnEnviarSugerencia.Size = new System.Drawing.Size(62, 23);
            this.btnEnviarSugerencia.TabIndex = 19;
            this.btnEnviarSugerencia.Text = "Enviar";
            this.btnEnviarSugerencia.UseVisualStyleBackColor = true;
            this.btnEnviarSugerencia.Click += new System.EventHandler(this.btnEnviarSugerencia_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 615);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 13);
            this.label1.TabIndex = 37;
            this.label1.Text = "Quejas o Sugerencias:";
            // 
            // btn_cancelar
            // 
            this.btn_cancelar.Location = new System.Drawing.Point(330, 89);
            this.btn_cancelar.Name = "btn_cancelar";
            this.btn_cancelar.Size = new System.Drawing.Size(75, 23);
            this.btn_cancelar.TabIndex = 38;
            this.btn_cancelar.Text = "Cancelar";
            this.btn_cancelar.UseVisualStyleBackColor = true;
            this.btn_cancelar.Click += new System.EventHandler(this.btn_cancelar_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(112, 292);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 39;
            this.label2.Text = "Encargado:";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(412, 292);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 40;
            this.label3.Text = "Encargado:";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(712, 292);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 13);
            this.label4.TabIndex = 41;
            this.label4.Text = "Encargado:";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1012, 292);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 42;
            this.label5.Text = "Encargado:";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(110, 518);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 43;
            this.label6.Text = "Encargado:";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(412, 518);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 13);
            this.label7.TabIndex = 44;
            this.label7.Text = "Encargado:";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(712, 518);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 13);
            this.label8.TabIndex = 45;
            this.label8.Text = "Encargado:";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(1012, 518);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 13);
            this.label9.TabIndex = 46;
            this.label9.Text = "Encargado:";
            // 
            // lblEncargadoCub1
            // 
            this.lblEncargadoCub1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEncargadoCub1.AutoSize = true;
            this.lblEncargadoCub1.Location = new System.Drawing.Point(114, 306);
            this.lblEncargadoCub1.Name = "lblEncargadoCub1";
            this.lblEncargadoCub1.Size = new System.Drawing.Size(175, 13);
            this.lblEncargadoCub1.TabIndex = 47;
            this.lblEncargadoCub1.Text = "____________________________";
            // 
            // lblEncargadoCub2
            // 
            this.lblEncargadoCub2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEncargadoCub2.AutoSize = true;
            this.lblEncargadoCub2.Location = new System.Drawing.Point(414, 306);
            this.lblEncargadoCub2.Name = "lblEncargadoCub2";
            this.lblEncargadoCub2.Size = new System.Drawing.Size(175, 13);
            this.lblEncargadoCub2.TabIndex = 48;
            this.lblEncargadoCub2.Text = "____________________________";
            // 
            // lblEncargadoCub3
            // 
            this.lblEncargadoCub3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEncargadoCub3.AutoSize = true;
            this.lblEncargadoCub3.Location = new System.Drawing.Point(714, 306);
            this.lblEncargadoCub3.Name = "lblEncargadoCub3";
            this.lblEncargadoCub3.Size = new System.Drawing.Size(175, 13);
            this.lblEncargadoCub3.TabIndex = 49;
            this.lblEncargadoCub3.Text = "____________________________";
            // 
            // lblEncargadoCub4
            // 
            this.lblEncargadoCub4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEncargadoCub4.AutoSize = true;
            this.lblEncargadoCub4.Location = new System.Drawing.Point(1016, 306);
            this.lblEncargadoCub4.Name = "lblEncargadoCub4";
            this.lblEncargadoCub4.Size = new System.Drawing.Size(175, 13);
            this.lblEncargadoCub4.TabIndex = 50;
            this.lblEncargadoCub4.Text = "____________________________";
            // 
            // lblEncargadoCub5
            // 
            this.lblEncargadoCub5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEncargadoCub5.AutoSize = true;
            this.lblEncargadoCub5.Location = new System.Drawing.Point(112, 531);
            this.lblEncargadoCub5.Name = "lblEncargadoCub5";
            this.lblEncargadoCub5.Size = new System.Drawing.Size(175, 13);
            this.lblEncargadoCub5.TabIndex = 51;
            this.lblEncargadoCub5.Text = "____________________________";
            // 
            // lblEncargadoCub6
            // 
            this.lblEncargadoCub6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEncargadoCub6.AutoSize = true;
            this.lblEncargadoCub6.Location = new System.Drawing.Point(414, 531);
            this.lblEncargadoCub6.Name = "lblEncargadoCub6";
            this.lblEncargadoCub6.Size = new System.Drawing.Size(175, 13);
            this.lblEncargadoCub6.TabIndex = 52;
            this.lblEncargadoCub6.Text = "____________________________";
            // 
            // lblEncargadoCub7
            // 
            this.lblEncargadoCub7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEncargadoCub7.AutoSize = true;
            this.lblEncargadoCub7.Location = new System.Drawing.Point(714, 531);
            this.lblEncargadoCub7.Name = "lblEncargadoCub7";
            this.lblEncargadoCub7.Size = new System.Drawing.Size(175, 13);
            this.lblEncargadoCub7.TabIndex = 53;
            this.lblEncargadoCub7.Text = "____________________________";
            // 
            // lblEncargadoCub8
            // 
            this.lblEncargadoCub8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEncargadoCub8.AutoSize = true;
            this.lblEncargadoCub8.Location = new System.Drawing.Point(1014, 531);
            this.lblEncargadoCub8.Name = "lblEncargadoCub8";
            this.lblEncargadoCub8.Size = new System.Drawing.Size(175, 13);
            this.lblEncargadoCub8.TabIndex = 54;
            this.lblEncargadoCub8.Text = "____________________________";
            // 
            // btnEditar1
            // 
            this.btnEditar1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditar1.Enabled = false;
            this.btnEditar1.Location = new System.Drawing.Point(112, 322);
            this.btnEditar1.Name = "btnEditar1";
            this.btnEditar1.Size = new System.Drawing.Size(44, 23);
            this.btnEditar1.TabIndex = 55;
            this.btnEditar1.Text = "Editar";
            this.btnEditar1.UseVisualStyleBackColor = true;
            this.btnEditar1.Click += new System.EventHandler(this.modificarCubiculos);
            // 
            // btnEditar2
            // 
            this.btnEditar2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditar2.Enabled = false;
            this.btnEditar2.Location = new System.Drawing.Point(419, 320);
            this.btnEditar2.Name = "btnEditar2";
            this.btnEditar2.Size = new System.Drawing.Size(44, 23);
            this.btnEditar2.TabIndex = 56;
            this.btnEditar2.Text = "Editar";
            this.btnEditar2.UseVisualStyleBackColor = true;
            this.btnEditar2.Click += new System.EventHandler(this.modificarCubiculos);
            // 
            // btnEditar3
            // 
            this.btnEditar3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditar3.Enabled = false;
            this.btnEditar3.Location = new System.Drawing.Point(716, 320);
            this.btnEditar3.Name = "btnEditar3";
            this.btnEditar3.Size = new System.Drawing.Size(44, 23);
            this.btnEditar3.TabIndex = 57;
            this.btnEditar3.Text = "Editar";
            this.btnEditar3.UseVisualStyleBackColor = true;
            this.btnEditar3.Click += new System.EventHandler(this.modificarCubiculos);
            // 
            // btnEditar4
            // 
            this.btnEditar4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditar4.Enabled = false;
            this.btnEditar4.Location = new System.Drawing.Point(1016, 320);
            this.btnEditar4.Name = "btnEditar4";
            this.btnEditar4.Size = new System.Drawing.Size(44, 23);
            this.btnEditar4.TabIndex = 58;
            this.btnEditar4.Text = "Editar";
            this.btnEditar4.UseVisualStyleBackColor = true;
            this.btnEditar4.Click += new System.EventHandler(this.modificarCubiculos);
            // 
            // btnEditar5
            // 
            this.btnEditar5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditar5.Enabled = false;
            this.btnEditar5.Location = new System.Drawing.Point(112, 544);
            this.btnEditar5.Name = "btnEditar5";
            this.btnEditar5.Size = new System.Drawing.Size(44, 23);
            this.btnEditar5.TabIndex = 59;
            this.btnEditar5.Text = "Editar";
            this.btnEditar5.UseVisualStyleBackColor = true;
            this.btnEditar5.Click += new System.EventHandler(this.modificarCubiculos);
            // 
            // btnEditar6
            // 
            this.btnEditar6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditar6.Enabled = false;
            this.btnEditar6.Location = new System.Drawing.Point(419, 547);
            this.btnEditar6.Name = "btnEditar6";
            this.btnEditar6.Size = new System.Drawing.Size(44, 23);
            this.btnEditar6.TabIndex = 60;
            this.btnEditar6.Text = "Editar";
            this.btnEditar6.UseVisualStyleBackColor = true;
            this.btnEditar6.Click += new System.EventHandler(this.modificarCubiculos);
            // 
            // btnEditar7
            // 
            this.btnEditar7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditar7.Enabled = false;
            this.btnEditar7.Location = new System.Drawing.Point(719, 547);
            this.btnEditar7.Name = "btnEditar7";
            this.btnEditar7.Size = new System.Drawing.Size(44, 23);
            this.btnEditar7.TabIndex = 61;
            this.btnEditar7.Text = "Editar";
            this.btnEditar7.UseVisualStyleBackColor = true;
            this.btnEditar7.Click += new System.EventHandler(this.modificarCubiculos);
            // 
            // btnEditar8
            // 
            this.btnEditar8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditar8.Enabled = false;
            this.btnEditar8.Location = new System.Drawing.Point(1019, 547);
            this.btnEditar8.Name = "btnEditar8";
            this.btnEditar8.Size = new System.Drawing.Size(44, 23);
            this.btnEditar8.TabIndex = 62;
            this.btnEditar8.Text = "Editar";
            this.btnEditar8.UseVisualStyleBackColor = true;
            this.btnEditar8.Click += new System.EventHandler(this.modificarCubiculos);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::Cubiculos.Properties.Resources.logoith21;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(233, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1031, 58);
            this.pictureBox1.TabIndex = 63;
            this.pictureBox1.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Lucida Sans Unicode", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(12, 188);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 34);
            this.label10.TabIndex = 64;
            this.label10.Text = "C";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Lucida Sans Unicode", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(12, 238);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(31, 34);
            this.label11.TabIndex = 65;
            this.label11.Text = "E";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Lucida Sans Unicode", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(12, 288);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(36, 34);
            this.label12.TabIndex = 66;
            this.label12.Text = "N";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Lucida Sans Unicode", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(12, 338);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(33, 34);
            this.label13.TabIndex = 67;
            this.label13.Text = "T";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Lucida Sans Unicode", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(12, 388);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(33, 34);
            this.label14.TabIndex = 68;
            this.label14.Text = "R";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Lucida Sans Unicode", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(12, 424);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(37, 34);
            this.label15.TabIndex = 69;
            this.label15.Text = "O";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Lucida Sans Unicode", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(46, 288);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(36, 34);
            this.label16.TabIndex = 70;
            this.label16.Text = "D";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Lucida Sans Unicode", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(42, 338);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(31, 34);
            this.label17.TabIndex = 71;
            this.label17.Text = "E";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Lucida Sans Unicode", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(80, 151);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(24, 34);
            this.label18.TabIndex = 72;
            this.label18.Text = "I";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Lucida Sans Unicode", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(76, 474);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(36, 34);
            this.label19.TabIndex = 73;
            this.label19.Text = "N";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Lucida Sans Unicode", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(74, 248);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(37, 34);
            this.label20.TabIndex = 74;
            this.label20.Text = "O";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Lucida Sans Unicode", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(76, 222);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(30, 34);
            this.label21.TabIndex = 75;
            this.label21.Text = "F";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Lucida Sans Unicode", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(77, 282);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(33, 34);
            this.label22.TabIndex = 76;
            this.label22.Text = "R";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Lucida Sans Unicode", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(76, 308);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(39, 34);
            this.label23.TabIndex = 77;
            this.label23.Text = "M";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Lucida Sans Unicode", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(78, 338);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(35, 34);
            this.label24.TabIndex = 78;
            this.label24.Text = "A";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Lucida Sans Unicode", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(78, 372);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(35, 34);
            this.label25.TabIndex = 79;
            this.label25.Text = "C";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Lucida Sans Unicode", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(82, 406);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(24, 34);
            this.label26.TabIndex = 80;
            this.label26.Text = "I";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Lucida Sans Unicode", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(75, 440);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(37, 34);
            this.label27.TabIndex = 81;
            this.label27.Text = "Ò";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Lucida Sans Unicode", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(74, 188);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(36, 34);
            this.label28.TabIndex = 82;
            this.label28.Text = "N";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1301, 733);
            this.ControlBox = false;
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnEditar8);
            this.Controls.Add(this.btnEditar7);
            this.Controls.Add(this.btnEditar6);
            this.Controls.Add(this.btnEditar5);
            this.Controls.Add(this.btnEditar4);
            this.Controls.Add(this.btnEditar3);
            this.Controls.Add(this.btnEditar2);
            this.Controls.Add(this.btnEditar1);
            this.Controls.Add(this.lblEncargadoCub8);
            this.Controls.Add(this.lblEncargadoCub7);
            this.Controls.Add(this.lblEncargadoCub6);
            this.Controls.Add(this.lblEncargadoCub5);
            this.Controls.Add(this.lblEncargadoCub4);
            this.Controls.Add(this.lblEncargadoCub3);
            this.Controls.Add(this.lblEncargadoCub2);
            this.Controls.Add(this.lblEncargadoCub1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btn_cancelar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnEnviarSugerencia);
            this.Controls.Add(this.txtSugerencia);
            this.Controls.Add(this.lblEncargado);
            this.Controls.Add(this.lblHora8);
            this.Controls.Add(this.lblHora7);
            this.Controls.Add(this.lblHora6);
            this.Controls.Add(this.lblHora5);
            this.Controls.Add(this.lblHora4);
            this.Controls.Add(this.lblHora3);
            this.Controls.Add(this.lblHora2);
            this.Controls.Add(this.lblHora1);
            this.Controls.Add(this.btnCancel8);
            this.Controls.Add(this.btnCancel7);
            this.Controls.Add(this.btnCancel6);
            this.Controls.Add(this.btnCancel5);
            this.Controls.Add(this.btnCancel4);
            this.Controls.Add(this.btnCancel3);
            this.Controls.Add(this.btnCancel2);
            this.Controls.Add(this.btnCancel1);
            this.Controls.Add(this.btnCubiculo3);
            this.Controls.Add(this.btnCubiculo8);
            this.Controls.Add(this.lblCarreraAlumno);
            this.Controls.Add(this.btnCubiculo1);
            this.Controls.Add(this.btnCubiculo7);
            this.Controls.Add(this.lblNombreAlumno);
            this.Controls.Add(this.btnCubiculo2);
            this.Controls.Add(this.btnSiguiente);
            this.Controls.Add(this.btnCubiculo6);
            this.Controls.Add(this.txtControl);
            this.Controls.Add(this.btnCubiculo4);
            this.Controls.Add(this.lblCarrera);
            this.Controls.Add(this.btnCubiculo5);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.lblControl);
            this.Controls.Add(this.msMenu);
            this.MainMenuStrip = this.msMenu;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "bibloteca";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.msMenu.ResumeLayout(false);
            this.msMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblControl;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label lblCarrera;
        private System.Windows.Forms.TextBox txtControl;
        private System.Windows.Forms.Button btnSiguiente;
        private System.Windows.Forms.Label lblCarreraAlumno;
        private System.Windows.Forms.Label lblNombreAlumno;
        private System.Windows.Forms.Button btnCubiculo1;
        private System.Windows.Forms.Button btnCubiculo2;
        private System.Windows.Forms.Button btnCubiculo3;
        private System.Windows.Forms.Button btnCubiculo4;
        private System.Windows.Forms.Button btnCubiculo5;
        private System.Windows.Forms.Button btnCubiculo6;
        private System.Windows.Forms.Button btnCubiculo7;
        private System.Windows.Forms.Button btnCubiculo8;
        private System.Windows.Forms.MenuStrip msMenu;
        private System.Windows.Forms.ToolStripMenuItem opcionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.Button btnCancel1;
        private System.Windows.Forms.Button btnCancel2;
        private System.Windows.Forms.Button btnCancel3;
        private System.Windows.Forms.Button btnCancel4;
        private System.Windows.Forms.Button btnCancel5;
        private System.Windows.Forms.Button btnCancel6;
        private System.Windows.Forms.Button btnCancel7;
        private System.Windows.Forms.Button btnCancel8;
        private System.Windows.Forms.Label lblHora1;
        private System.Windows.Forms.Label lblHora2;
        private System.Windows.Forms.Label lblHora3;
        private System.Windows.Forms.Label lblHora4;
        private System.Windows.Forms.Label lblHora5;
        private System.Windows.Forms.Label lblHora6;
        private System.Windows.Forms.Label lblHora7;
        private System.Windows.Forms.Label lblHora8;
        private System.Windows.Forms.ToolStripMenuItem tmiEncargados;
        private System.Windows.Forms.ToolStripMenuItem tmiAlumnos;
        public System.Windows.Forms.ToolStripMenuItem tmiAdministrador;
        public System.Windows.Forms.Label lblEncargado;
        private System.Windows.Forms.TextBox txtSugerencia;
        private System.Windows.Forms.Button btnEnviarSugerencia;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem tmiReportes;
        private System.Windows.Forms.Button btn_cancelar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblEncargadoCub1;
        private System.Windows.Forms.Label lblEncargadoCub2;
        private System.Windows.Forms.Label lblEncargadoCub3;
        private System.Windows.Forms.Label lblEncargadoCub4;
        private System.Windows.Forms.Label lblEncargadoCub5;
        private System.Windows.Forms.Label lblEncargadoCub6;
        private System.Windows.Forms.Label lblEncargadoCub7;
        private System.Windows.Forms.Label lblEncargadoCub8;
        private System.Windows.Forms.Button btnEditar1;
        private System.Windows.Forms.Button btnEditar2;
        private System.Windows.Forms.Button btnEditar3;
        private System.Windows.Forms.Button btnEditar4;
        private System.Windows.Forms.Button btnEditar5;
        private System.Windows.Forms.Button btnEditar6;
        private System.Windows.Forms.Button btnEditar7;
        private System.Windows.Forms.Button btnEditar8;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
    }
}

