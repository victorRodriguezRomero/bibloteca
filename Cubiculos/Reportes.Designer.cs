﻿namespace Cubiculos
{
    partial class Reportes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtp_intervaloF = new System.Windows.Forms.DateTimePicker();
            this.btn_printR = new System.Windows.Forms.Button();
            this.dtp_intervaloI = new System.Windows.Forms.DateTimePicker();
            this.dGV_reportes = new System.Windows.Forms.DataGridView();
            this.clave = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Carrera = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nAlumnos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dGV_reportes)).BeginInit();
            this.SuspendLayout();
            // 
            // dtp_intervaloF
            // 
            this.dtp_intervaloF.CustomFormat = "yyyy-MM-dd";
            this.dtp_intervaloF.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_intervaloF.Location = new System.Drawing.Point(552, 47);
            this.dtp_intervaloF.Name = "dtp_intervaloF";
            this.dtp_intervaloF.Size = new System.Drawing.Size(92, 20);
            this.dtp_intervaloF.TabIndex = 7;
            this.dtp_intervaloF.Value = new System.DateTime(2017, 6, 21, 9, 8, 47, 0);
            this.dtp_intervaloF.ValueChanged += new System.EventHandler(this.dtp_intervaloF_ValueChanged_1);
            // 
            // btn_printR
            // 
            this.btn_printR.Location = new System.Drawing.Point(520, 387);
            this.btn_printR.Name = "btn_printR";
            this.btn_printR.Size = new System.Drawing.Size(75, 23);
            this.btn_printR.TabIndex = 6;
            this.btn_printR.Text = "Generar";
            this.btn_printR.UseVisualStyleBackColor = true;
            this.btn_printR.Click += new System.EventHandler(this.btn_printR_Click_1);
            // 
            // dtp_intervaloI
            // 
            this.dtp_intervaloI.CustomFormat = "yyyy-MM-dd";
            this.dtp_intervaloI.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_intervaloI.Location = new System.Drawing.Point(552, 21);
            this.dtp_intervaloI.Name = "dtp_intervaloI";
            this.dtp_intervaloI.Size = new System.Drawing.Size(92, 20);
            this.dtp_intervaloI.TabIndex = 4;
            this.dtp_intervaloI.Value = new System.DateTime(2017, 6, 20, 0, 0, 0, 0);
            this.dtp_intervaloI.ValueChanged += new System.EventHandler(this.dtp_intervaloI_ValueChanged_1);
            // 
            // dGV_reportes
            // 
            this.dGV_reportes.AllowUserToAddRows = false;
            this.dGV_reportes.AllowUserToDeleteRows = false;
            this.dGV_reportes.AllowUserToResizeColumns = false;
            this.dGV_reportes.AllowUserToResizeRows = false;
            this.dGV_reportes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dGV_reportes.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dGV_reportes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGV_reportes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clave,
            this.Carrera,
            this.nAlumnos});
            this.dGV_reportes.Location = new System.Drawing.Point(15, 52);
            this.dGV_reportes.Name = "dGV_reportes";
            this.dGV_reportes.ReadOnly = true;
            this.dGV_reportes.Size = new System.Drawing.Size(481, 358);
            this.dGV_reportes.TabIndex = 5;
            this.dGV_reportes.TabStop = false;
            // 
            // clave
            // 
            this.clave.HeaderText = "Clave Carrera";
            this.clave.Name = "clave";
            this.clave.ReadOnly = true;
            this.clave.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Carrera
            // 
            this.Carrera.HeaderText = "Carrera";
            this.Carrera.Name = "Carrera";
            this.Carrera.ReadOnly = true;
            this.Carrera.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // nAlumnos
            // 
            this.nAlumnos.HeaderText = "Cantidad de Alumnos";
            this.nAlumnos.Name = "nAlumnos";
            this.nAlumnos.ReadOnly = true;
            this.nAlumnos.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Reportes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(658, 430);
            this.Controls.Add(this.dtp_intervaloF);
            this.Controls.Add(this.btn_printR);
            this.Controls.Add(this.dtp_intervaloI);
            this.Controls.Add(this.dGV_reportes);
            this.Name = "Reportes";
            this.Text = "Reportes";
            ((System.ComponentModel.ISupportInitialize)(this.dGV_reportes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtp_intervaloF;
        private System.Windows.Forms.Button btn_printR;
        private System.Windows.Forms.DateTimePicker dtp_intervaloI;
        private System.Windows.Forms.DataGridView dGV_reportes;
        private System.Windows.Forms.DataGridViewTextBoxColumn clave;
        private System.Windows.Forms.DataGridViewTextBoxColumn Carrera;
        private System.Windows.Forms.DataGridViewTextBoxColumn nAlumnos;
    }
}