﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cubiculos
{
    public partial class Prestamo : Form
    {

        Form1 principal;
        Consultas cons;
        int cubiculo, alumno, encargado;
        string carrera;
        DateTime Hora;
        PrestamoNodo nodo;

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Dispose();
        }

        public Prestamo(Form1 principal, int cubiculo, int alumno, int encargado, string carrera)
        {
            InitializeComponent();
            this.principal = principal;
            this.cubiculo = cubiculo;
            this.alumno = alumno;
            this.encargado = encargado;
            this.carrera = carrera;
            Hora = DateTime.Now;
            cons = new Consultas();
            hora();
        }

        private void hora()
        {
            if (DateTime.Now.Hour < 12)
            {
                if (DateTime.Now.Minute < 10)
                {
                    lblHora.Text = DateTime.Now.Hour + ":0" + DateTime.Now.Minute + " AM";
                }
                else
                {
                    lblHora.Text = DateTime.Now.Hour + ":" + DateTime.Now.Minute + " AM";
                }
            }
            else if (DateTime.Now.Hour == 12)
            {
                if (DateTime.Now.Minute < 10)
                {
                    lblHora.Text = DateTime.Now.Hour + ":0" + DateTime.Now.Minute + " PM";
                }
                else
                {
                    lblHora.Text = DateTime.Now.Hour + ":" + DateTime.Now.Minute + " PM";
                }
            }
            else
            {
                if (DateTime.Now.Minute < 10)
                {
                    lblHora.Text = DateTime.Now.Hour + ":0" + DateTime.Now.Minute + " PM";
                }
                else
                {
                    lblHora.Text = DateTime.Now.Hour + ":" + DateTime.Now.Minute + " PM";
                }
            }
        }

        private void nudPersonas_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnOk_Click(null, null);
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            char presta = ' ';

            if (ckbPrestamo.Checked == true)
            {
                presta = 'Y';
            }
            else
            {
                presta = 'N';
            }

            if (nudPersonas.Value >= 1 && nudPersonas.Value <= 8)
            {
                nodo = new PrestamoNodo((int)nudPersonas.Value, cubiculo, Hora, presta, alumno, encargado, carrera);
                principal.cabeza = cons.insertaNodo(nodo, principal.cabeza);
                MessageBox.Show("Puede pasar al cubiculo " + cubiculo);
                principal.eliminaIDAlumno();
                principal.modificaEncargadocub(cubiculo, cons.obtenNombre(alumno.ToString()));
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("Revise el numero de personas.\n \t Minimo 4 \n \t Maximo 8");
            }
        }

        private void nudPersonas_ValueChanged(object sender, EventArgs e)
        {
            if (nudPersonas.Value < 1)
                nudPersonas.Value = 1;

            if (nudPersonas.Value > 8)
                nudPersonas.Value = 8;
        }
    }
}
