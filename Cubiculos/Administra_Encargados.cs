﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cubiculos
{
    public partial class Administra_Encargados : Form
    {
        Consultas cons;
        Form1 principal;

        public Administra_Encargados(Form1 principal)
        {
            InitializeComponent();
            cons = new Consultas();
            txt_ide.Focus();
            this.principal = principal;
        }

        private void Administra_Encargados_Load(object sender, EventArgs e)
        {
            cBA_nivel.SelectedIndex = 0;
        }

        private void btnA_agregar_Click_1(object sender, EventArgs e)
        {
            if (tBA_pass.Text == txt_pass2.Text)
            {
                try
                {
                    string mensaje = cons.Agregar_Encargado(Convert.ToInt32(txt_ide.Text), tBA_nombre.Text, tBA_pass.Text, (string)cBA_nivel.SelectedItem);
                    MessageBox.Show(mensaje, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txt_ide.Focus();
                    txt_ide.Clear();
                    txt_pass2.Clear();
                    tBA_nombre.Clear();
                    tBA_pass.Clear();
                    cBA_nivel.SelectedIndex = -1;
                }
                catch (Exception)
                {
                    MessageBox.Show("Revise los datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txt_ide.Focus();
                    txt_pass2.Clear();
                    txt_ide.Clear();
                    tBA_nombre.Clear();
                    tBA_pass.Clear();
                    cBA_nivel.SelectedIndex = -1;
                }
            }
            else
            {
                MessageBox.Show("La contraseña no coincide en ambos campos.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                tBA_pass.Focus();
                txt_pass2.Clear();
            }

        }

        private void btnA_salir_Click_1(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void Administra_Encargados_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!cons.obtenEncargado(txtControl.Text).Equals(null))
                {
                    lblNombreE.Text = cons.obtenEncargado(txtControl.Text);
                    lblconE.Text = "*****";
                    txtControl.Enabled = false;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("ID de encargado invalida.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtControl.Clear();
                lblNombreE.Text = "__________________";
                lblconE.Text = "__________________";
                txtControl.Focus();
            }
        }

        private void bntLimpiar_Click(object sender, EventArgs e)
        {
            txtControl.Clear();
            lblNombreE.Text = "__________________";
            lblconE.Text = "__________________";
            txtControl.Focus();
            txtControl.Enabled = true;
        }

        private void btn_elime_Click(object sender, EventArgs e)
        {
            ValidaEncargado validar = new ValidaEncargado(principal,0);
            if (validar.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string mensaje = cons.Eliminar_Encargado(Convert.ToInt32(txtControl.Text));
                    MessageBox.Show(mensaje, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    bntLimpiar_Click(null, null);
                }
                catch (Exception)
                {
                    MessageBox.Show("Revise los datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtControl.Clear();
                    lblNombreE.Text = "__________________";
                    lblconE.Text = "__________________";
                    txtControl.Focus();
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                if (!cons.obtenEncargado(textBox1.Text).Equals(null))
                {
                    label2.Text = cons.obtenEncargado(textBox1.Text);
                    textBox5.Text = cons.obtenPass(Convert.ToInt32(textBox1.Text));
                    textBox1.Enabled = false;
                    groupBox2.Visible = true;
                    lblControl.Text = textBox1.Text;
                    txtNombreModifica.Text = label2.Text;

                }
            }
            catch (Exception)
            {
                MessageBox.Show("ID de encargado invalida.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBox1.Clear();
                label2.Text = "__________________";
                textBox5.Clear();
                textBox1.Focus();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            lblControl.Text = "ID";
            txtNombreModifica.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            textBox1.Clear();
            label2.Text = "__________________";
            textBox5.Clear();
            groupBox2.Visible = false;
            textBox1.Focus();
            textBox1.Enabled = true;
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (textBox5.Text == textBox2.Text)
            {
                if (textBox3.Text != "" || textBox4.Text != "")
                {
                    if (textBox3.Text == textBox4.Text)
                    {
                        string mensaje = cons.Modificar_Encargado(Convert.ToInt32(lblControl.Text), txtNombreModifica.Text, textBox4.Text);
                        MessageBox.Show(mensaje, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        button1_Click(null, null);
                    }
                    else
                    {
                        MessageBox.Show("La nueva contrasena no coincide en ambos campos.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox4.Clear();
                        textBox3.Focus();
                    }
                }
                else
                {
                    string mensaje = cons.Modificar_EncargadoN(Convert.ToInt32(lblControl.Text), txtNombreModifica.Text);
                    MessageBox.Show(mensaje, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblControl.Text = "ID";
                    txtNombreModifica.Clear();
                    textBox2.Clear();
                    textBox3.Clear();
                    textBox4.Clear();
                    textBox1.Clear();
                    label2.Text = "__________________";
                    textBox5.Clear();
                    groupBox2.Visible = false;
                    textBox1.Focus();
                    textBox1.Enabled = true;
                }
            }
            else
            {
                MessageBox.Show("La contrasena actual no coincide.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBox2.Clear();
                textBox2.Focus();
            }
        }

        private void txt_ide_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnA_agregar_Click_1(this, new EventArgs());
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button3_Click(this, new EventArgs());
            }
        }

        private void txtNombreModifica_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnModificar_Click(this, new EventArgs());
            }
        }

        private void txtControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnBuscar_Click(this, new EventArgs());
            }
        }

        private void bntLimpiar_Click_1(object sender, EventArgs e)
        {
            txtControl.Clear();
            lblNombreE.Text = "__________________";
            lblconE.Text = "__________________";
            txtControl.Enabled = true;
        }
    }
}