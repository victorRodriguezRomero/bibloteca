﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cubiculos
{
    public partial class Modificar_Cubiculo : Form
    {
        Form1 principal;
        int cubiculo;
        PrestamoNodo P;
        Consultas cons = new Consultas();
        bool alumno = false;

        public Modificar_Cubiculo(Form1 principal, int cubiculo)
        {
            InitializeComponent();
            this.principal = principal;
            this.cubiculo = cubiculo;
            P = principal.cabeza;
            etiquetas();
        }

        private void etiquetas()
        {
            try
            {
                if (P.cubiculo == cubiculo)
                {
                    lblControl.Text = P.id_alumno.ToString();
                    lblNombre.Text = cons.obtenNombre(P.id_alumno.ToString());
                    lblCarrera.Text = P.carrera;
                    lblNPersonas.Text = P.personas.ToString();
                    etiquetaHora(P.entrada);
                    if(P.prestamo == 'Y')
                    {
                        cbxMarcadoresActual.CheckState = CheckState.Checked;
                    }
                    else
                    {
                        cbxMarcadoresActual.CheckState = CheckState.Unchecked;
                    }
                }
                else
                {
                    while (P.sig != null)
                    {
                        P = P.sig;
                        if (P.cubiculo == cubiculo)
                        {
                            lblControl.Text = P.id_alumno.ToString();
                            lblNombre.Text = cons.obtenNombre(P.id_alumno.ToString());
                            lblCarrera.Text = P.carrera;
                            lblNPersonas.Text = P.personas.ToString();
                            etiquetaHora(P.entrada);
                            if (P.prestamo == 'Y')
                            {
                                cbxMarcadoresActual.CheckState = CheckState.Checked;
                            }
                            else
                            {
                                cbxMarcadoresActual.CheckState = CheckState.Unchecked;
                            }
                        }
                    }
                }
            }
            catch
            {

            }
        }

        private void etiquetaHora(DateTime entrada)
        {
            DateTime salida = entrada;
            salida = salida.AddHours(2);

            if (entrada.Hour < 12)
            {
                if (entrada.Minute < 10)
                {
                    lblEntrada.Text = entrada.Hour + ":0" + entrada.Minute + " AM";
                }
                else
                {
                    lblEntrada.Text = entrada.Hour + ":" + entrada.Minute + " AM";
                }
            }
            else if (entrada.Hour == 12)
            {
                if (entrada.Minute < 10)
                {
                    lblEntrada.Text = entrada.Hour + ":0" + entrada.Minute + " PM";
                }
                else
                {
                    lblEntrada.Text = entrada.Hour + ":" + entrada.Minute + " PM";
                }
            }
            else
            {
                if (entrada.Minute < 10)
                {
                    lblEntrada.Text = entrada.Hour + ":0" + entrada.Minute + " PM";
                }
                else
                {
                    lblEntrada.Text = entrada.Hour + ":" + entrada.Minute + " PM";
                }
            }

            if (salida.Hour < 12)
            {
                if (salida.Minute < 10)
                {
                    lblSalida.Text = salida.Hour + ":0" + salida.Minute + " AM";
                }
                else
                {
                    lblSalida.Text = salida.Hour + ":" + salida.Minute + " AM";
                }
            }
            else if (salida.Hour == 12)
            {
                if (salida.Minute < 10)
                {
                    lblSalida.Text = salida.Hour + ":0" + salida.Minute + " PM";
                }
                else
                {
                    lblSalida.Text = salida.Hour + ":" + salida.Minute + " PM";
                }
            }
            else
            {
                if (salida.Minute < 10)
                {
                    lblSalida.Text = salida.Hour + ":0" + salida.Minute + " PM";
                }
                else
                {
                    lblSalida.Text = salida.Hour + ":" + salida.Minute + " PM";
                }
            }
        }

        private void btnSiguiente_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtControl.Text != lblControl.Text && principal.alumnoExistente(Convert.ToInt32(txtControl.Text)))
                {
                    if (!cons.obtenNombre(txtControl.Text).Equals("nell"))
                    {
                        lblNombreAlumno.Text = cons.obtenNombre(txtControl.Text);
                        lblCarreraAlumno.Text = cons.obtenCarrera(txtControl.Text);
                        txtControl.Enabled = false;
                        alumno = true;
                    }
                }
                else
                {
                    MessageBox.Show("El alumno " + cons.obtenNombre(txtControl.Text) + " ya está ocupando el cubículo " + principal.cubiculo_ocupado); ;
                }
            }
            catch
            {
                MessageBox.Show("No existe alumno con ese numero de control o no está registrado.");
            }
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            txtControl.Clear();
            lblNombreAlumno.Text = "";
            lblCarreraAlumno.Text = "";
            txtControl.Enabled = true;
            txtControl.Focus();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (alumno)
            {
                P = principal.cabeza;
                if (P.cubiculo == cubiculo)
                {
                    P.id_alumno = Convert.ToInt32(txtControl.Text);
                    P.carrera = lblCarreraAlumno.Text;
                    if (cbxMarcadores.Checked)
                    {
                        P.prestamo = 'Y';
                    }
                    else
                    {
                        P.prestamo = 'N';
                    }
                    principal.modificaEncargadocub(cubiculo, lblNombreAlumno.Text);
                }
                else
                {
                    while (P.sig != null)
                    {
                        P = P.sig;
                        if (P.cubiculo == cubiculo)
                        {
                            P.id_alumno = Convert.ToInt32(txtControl.Text);
                            P.carrera = lblCarreraAlumno.Text;
                            if (cbxMarcadores.Checked)
                            {
                                P.prestamo = 'Y';
                            }
                            else
                            {
                                P.prestamo = 'N';
                            }
                            principal.modificaEncargadocub(cubiculo, lblNombreAlumno.Text);
                        }
                    }
                }
                MessageBox.Show("El encargado del cubiculo " + cubiculo + " fue modificado exitosamente");
                principal.eliminaIDAlumno();
                this.Dispose();
            }
            else
            {
                P = principal.cabeza;
                if (P.cubiculo == cubiculo)
                {
                    if (cbxMarcadores.Checked)
                    {
                        P.prestamo = 'Y';
                    }
                    else
                    {
                        P.prestamo = 'N';
                    }
                }
                else
                {
                    while (P.sig != null)
                    {
                        P = P.sig;
                        if (P.cubiculo == cubiculo)
                        {
                            if (cbxMarcadores.Checked)
                            {
                                P.prestamo = 'Y';
                            }
                            else
                            {
                                P.prestamo = 'N';
                            }
                        }
                    }
                }
                principal.eliminaIDAlumno();
                this.Dispose();
            }
        }

        private void txtControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSiguiente_Click(this, new EventArgs());
            }
        }
    }
}
