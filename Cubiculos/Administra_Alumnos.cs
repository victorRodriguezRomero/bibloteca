﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cubiculos
{
    public partial class Administra_Alumnos : Form
    {
        Consultas cons = new Consultas();
        int[] carreras;
        Form1 principal;


        public Administra_Alumnos(Form1 principal)
        {
            InitializeComponent();
            LlenaCarreras();
            this.principal = principal;
        }

        void LlenaCarreras()
        {
            string[] nombre_carrera;
            List<object> lista = cons.LlenarCarreras();

            nombre_carrera = (string[])lista[0];
            carreras = (int[])lista[1];

            cb_Carreras.Items.Clear();
            comboBox1.Items.Clear();

            for (int i = 0; i < nombre_carrera.Count(); i++)
            {
                cb_Carreras.Items.Add(nombre_carrera[i]);
                comboBox1.Items.Add(nombre_carrera[i]);
            }
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            if (cb_Carreras.SelectedIndex >= 0)
            {
                if (txtNumeroControl.Text.Length == 8)
                {
                    try
                    {
                        MessageBox.Show(cons.Agregar_Alumno(Convert.ToInt32(txtNumeroControl.Text), txtNombreAgregar.Text, carreras[cb_Carreras.SelectedIndex]));
                        txtNumeroControl.Clear();
                        txtNombreAgregar.Clear();
                        cb_Carreras.SelectedIndex = -1;
                    }
                    catch (Exception x)
                    {
                        MessageBox.Show("Revise los datos");
                    }
                }
                else
                {
                    MessageBox.Show("El numero de control de un alumno debe ser de 8 digitos");
                }
            }
            else
            {
                MessageBox.Show("Seleccione Carrera");
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!cons.obtenNombre(txtControl.Text).Equals("nell"))
                {
                    lblNombreAlumno.Text = cons.obtenNombre(txtControl.Text);
                    lblCarreraAlumno.Text = cons.obtenCarrera(txtControl.Text);
                    txtControl.Enabled = false;
                    groupBox2.Visible = true;
                    lblControl.Text = txtControl.Text;
                    txtNombreModifica.Text = lblNombreAlumno.Text;
                }
            }
            catch
            {
                MessageBox.Show("No existe alumno con ese numero de control o no está registrado.");
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex >= 0)
            {
                MessageBox.Show(cons.Modificar_Alumno(Convert.ToInt32(txtControl.Text), txtNombreModifica.Text, carreras[comboBox1.SelectedIndex]));
                btnLimpiar_Click(null, null);
            }
            else
            {
                MessageBox.Show("Selecciona una carrera");
            }
        }

        private void btnBuscarEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!cons.obtenNombre(txtControlEliminar.Text).Equals("nell"))
                {
                    lblAlumnoEliminar.Text = cons.obtenNombre(txtControlEliminar.Text);
                    lblCarreraEliminar.Text = cons.obtenCarrera(txtControlEliminar.Text);
                    txtControlEliminar.Enabled = false;
                }
            }
            catch
            {
                MessageBox.Show("No existe alumno con ese numero de control o no está registrado.");
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            ValidaEncargado validar = new ValidaEncargado(principal,0);
            if (validar.ShowDialog() == DialogResult.OK)
            {
                MessageBox.Show(cons.Eliminar_Alumno(Convert.ToInt32(txtControlEliminar.Text), lblAlumnoEliminar.Text));
            }
            btnLimpiarEliminar_Click(null, null);
        }

        private void btnLimpiarEliminar_Click(object sender, EventArgs e)
        {
            lblAlumnoEliminar.Text = "__________________";
            lblCarreraEliminar.Text = "__________________";
            txtControlEliminar.Clear();
            txtControlEliminar.Enabled = true;
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            lblCarreraAlumno.Text = "__________________";
            lblNombreAlumno.Text = "__________________";
            txtControl.Clear();
            txtControl.Enabled = true;
            lblControl.Text = "";
            txtNombreModifica.Clear();
            groupBox2.Visible = false;
            comboBox1.SelectedIndex = -1;
            comboBox1.Text = "Selecciona Carrera";
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtNumeroControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnInsertar_Click(this, new EventArgs());
            }
        }

        private void txtControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnBuscar_Click(this, new EventArgs());
            }
        }

        private void txtNombreModifica_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnModificar_Click(this, new EventArgs());
            }
        }

        private void txtControlEliminar_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnBuscarEliminar_Click(this, new EventArgs());
            }
        }
    }
}
