﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;


namespace Cubiculos
{
    public partial class Form1 : Form
    {
        int tiempo = 7200000;
        public PrestamoNodo cabeza;
        public int id_encargado;
        public int cubiculo_ocupado;
        Consultas cons;
        Thread cb1, cb2, cb3, cb4, cb5, cb6, cb7, cb8;
        ThreadStart childref;
        bool prestamo;
        Login login;

        public Form1(Login login)
        {
            InitializeComponent();
            prestamo = false;
            cons = new Consultas();
            this.login = login;
            childref = new ThreadStart(Timer);
            iniciaHilos();
            lblNombreAlumno.Enabled = true;
            lblCarreraAlumno.Visible = true;
        }

        private void messagebox(string mensaje)
        {
            MessageBox.Show(mensaje, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        public void eliminaIDAlumno()
        {
            txtControl.Clear();
            lblNombreAlumno.Text = "";
            lblCarreraAlumno.Text = "";
            prestamo = false;
            txtControl.Enabled = true;
            txtControl.Focus();
        }

        public void cancelaPrestamo(int cubiculo)
        {
            switch (cubiculo)
            {
                case 1:
                    cb1.Abort();
                    cb1 = new Thread(childref);
                    btnCubiculo1.BackColor = Color.YellowGreen;
                    break;
                case 2:
                    cb2.Abort();
                    cb2 = new Thread(childref);
                    btnCubiculo2.BackColor = Color.YellowGreen;
                    break;
                case 3:
                    cb3.Abort();
                    cb3 = new Thread(childref);
                    btnCubiculo3.BackColor = Color.YellowGreen;
                    break;
                case 4:
                    cb4.Abort();
                    cb4 = new Thread(childref);
                    btnCubiculo4.BackColor = Color.YellowGreen;
                    break;
                case 5:
                    cb5.Abort();
                    cb5 = new Thread(childref);
                    btnCubiculo5.BackColor = Color.Brown;
                    break;
                case 6:
                    cb6.Abort();
                    cb6 = new Thread(childref);
                    btnCubiculo6.BackColor = Color.YellowGreen;
                    break;
                case 7:
                    cb7.Abort();
                    cb7 = new Thread(childref);
                    btnCubiculo7.BackColor = Color.YellowGreen;
                    break;
                case 8:
                    cb8.Abort();
                    cb8 = new Thread(childref);
                    btnCubiculo8.BackColor = Color.YellowGreen;
                    break;
            }
        }

        public void inicialbl(int cubiculo)
        {
            string texto = "Hora de termino: " + DateTime.Now.ToShortTimeString();
            string encargado = "________________________";
            switch (cubiculo)
            {
                case 1:
                    lblHora1.Text = texto;
                    lblEncargadoCub1.Text = encargado;
                    btnEditar1.Enabled = false;
                    break;
                case 2:
                    lblHora2.Text = texto;
                    lblEncargadoCub2.Text = encargado;
                    btnEditar2.Enabled = false;
                    break;
                case 3:
                    lblHora3.Text = texto;
                    lblEncargadoCub3.Text = encargado;
                    btnEditar3.Enabled = false;
                    break;
                case 4:
                    lblHora4.Text = texto;
                    lblEncargadoCub4.Text = encargado;
                    btnEditar4.Enabled = false;
                    break;
                case 5:
                    lblHora5.Text = texto;
                    lblEncargadoCub5.Text = encargado;
                    btnEditar5.Enabled = false;
                    break;
                case 6:
                    lblHora6.Text = texto;
                    lblEncargadoCub6.Text = encargado;
                    btnEditar6.Enabled = false;
                    break;
                case 7:
                    lblHora7.Text = texto;
                    lblEncargadoCub7.Text = encargado;
                    btnEditar7.Enabled = false;
                    break;
                case 8:
                    lblHora8.Text = texto;
                    lblEncargadoCub8.Text = encargado;
                    btnEditar8.Enabled = false;
                    break;
            }
            txtControl.Focus();
        }

        public void registraPrestamo(int cubiculo)
        {
            try
            {
                PrestamoNodo P = cabeza;
                PrestamoNodo Q = P;
                if (P.sig == null)
                {
                    cabeza = null;
                }
                else
                    while (P.sig != null)
                    {
                        if (P.cubiculo == cubiculo)
                        {
                            Q = P;
                        }
                        P = P.sig;
                    }
                string mensaje = cons.insertaPrestamoCubiculo(Q.personas, Q.cubiculo, Q.entrada, Q.prestamo, Q.id_alumno, Q.id_encargado, Q.carrera);
                if (!mensaje.Equals(""))
                {
                    MessageBox.Show(mensaje);
                }
                else
                    cabeza = cons.eliminaNodo(cubiculo, cabeza);

            }
            catch
            {
                MessageBox.Show("El cubiculo " + cubiculo + " está libre.");
            }
            finally
            {
                inicialbl(cubiculo);
            }
        }

        public bool validaEncargado(string pass)
        {
            if (pass.Equals(cons.obtenPass(id_encargado)))
                return true;

            else
                return false;
        }

        public void modificaEncargadocub(int cubiculo, string actual)
        {
            switch (cubiculo)
            {
                case 1:
                    lblEncargadoCub1.Text = actual;
                    break;
                case 2:
                    lblEncargadoCub2.Text = actual;
                    break;
                case 3:
                    lblEncargadoCub3.Text = actual;
                    break;
                case 4:
                    lblEncargadoCub4.Text = actual;
                    break;
                case 5:
                    lblEncargadoCub5.Text = actual;
                    break;
                case 6:
                    lblEncargadoCub6.Text = actual;
                    break;
                case 7:
                    lblEncargadoCub7.Text = actual;
                    break;
                case 8:
                    lblEncargadoCub8.Text = actual;
                    break;
            }
        }

        public bool alumnoExistente(int id)
         {
            PrestamoNodo P;

            P = cabeza;

            if (cabeza == null)
                return true;

            while (P != null)
            {
                if (P.id_alumno == id)
                {
                    cubiculo_ocupado = P.cubiculo;
                    return false;
                }
                P = P.sig;
            }
            return true;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            login.Visible = true;
        }

        private void iniciaHilos()
        {
            cb1 = new Thread(childref);
            cb2 = new Thread(childref);
            cb3 = new Thread(childref);
            cb4 = new Thread(childref);
            cb5 = new Thread(childref);
            cb6 = new Thread(childref);
            cb7 = new Thread(childref);
            cb8 = new Thread(childref);
        }

        private void btnSiguiente_Click(object sender, EventArgs e)
        {
            try
            {
                if (!cons.obtenNombre(txtControl.Text).Equals("nell"))
                {
                    lblNombreAlumno.Text = cons.obtenNombre(txtControl.Text);
                    lblCarreraAlumno.Text = cons.obtenCarrera(txtControl.Text);
                    txtControl.Enabled = false;
                }
                if (alumnoExistente(Convert.ToInt32(txtControl.Text)))
                {
                    prestamo = true;
                }
                else
                {
                    MessageBox.Show("El alumno " + lblNombreAlumno.Text + " ya tiene ocupado el cubiculo " + cubiculo_ocupado);
                    eliminaIDAlumno();
                }
            }
            catch
            {
                MessageBox.Show("No existe alumno con ese numero de control o no está registrado.");
            }
        }

        private void tmiAlumnos_Click(object sender, EventArgs e)
        {
            ValidaEncargado validar = new ValidaEncargado(this,0);
            if(validar.ShowDialog() == DialogResult.OK)
            {
                Administra_Alumnos alumnos = new Administra_Alumnos(this);
                alumnos.ShowDialog();
            }
        }

        private void btnEnviarSugerencia_Click(object sender, EventArgs e)
        {
            if(!txtSugerencia.Text.Equals(""))
            {
                MessageBox.Show(cons.Enviar_Sugerencia(txtSugerencia.Text));
            }

            txtSugerencia.Clear();
        }

        private void IniciaTimer(object sender, EventArgs e)
        {
            if (prestamo)
            {
                Prestamo pantallaPrestamo;
                Button boton = (Button)sender;
                string cubiculo = boton.Name.Substring(boton.Name.Length - 1);
                string ocupado = "El cubiculo " + cubiculo +" ya está ocupado";
                pantallaPrestamo = new Prestamo(this, Convert.ToInt32(cubiculo), Convert.ToInt32(txtControl.Text), id_encargado, lblCarreraAlumno.Text);
                #region Crea Hilos
                switch (cubiculo)
                {
                    case "1":
                        if ((cb1.ThreadState.Equals(ThreadState.Stopped) || cb1.ThreadState.Equals(ThreadState.Unstarted)) && pantallaPrestamo.ShowDialog() == DialogResult.OK)
                        {
                            cb1.Abort();
                            cb1 = new Thread(childref);
                            lblHora1.Text = "Hora inicial: " + DateTime.Now.ToShortTimeString();
                            btnEditar1.Enabled = true;
                            prestamo = false;
                            cb1.Start();
                            boton.BackColor = Color.Red;
                        }
                        else
                        {
                            if (cb1.ThreadState.Equals(ThreadState.WaitSleepJoin))
                            {
                                messagebox(ocupado);
                            }
                        }
                        break;
                    case "2":
                        if ((cb2.ThreadState.Equals(ThreadState.Stopped) || cb2.ThreadState.Equals(ThreadState.Aborted) || cb2.ThreadState.Equals(ThreadState.Unstarted)) && pantallaPrestamo.ShowDialog() == DialogResult.OK)
                        {
                            cb2 = new Thread(childref);
                            lblHora2.Text = "Hora inicial: " + DateTime.Now.ToShortTimeString();
                            btnEditar2.Enabled = true;
                            prestamo = false;
                            cb2.Start();
                            boton.BackColor = Color.Red;
                        }
                        else
                        {
                            if (cb2.ThreadState.Equals(ThreadState.WaitSleepJoin))
                            {
                                messagebox(ocupado);
                            }
                        }
                        break;
                    case "3":
                        if ((cb3.ThreadState.Equals(ThreadState.Stopped) || cb3.ThreadState.Equals(ThreadState.Aborted) || cb3.ThreadState.Equals(ThreadState.Unstarted)) && pantallaPrestamo.ShowDialog() == DialogResult.OK)
                        {
                            cb3 = new Thread(childref);
                            lblHora3.Text = "Hora inicial: " + DateTime.Now.ToShortTimeString();
                            btnEditar3.Enabled = true;
                            prestamo = false;
                            cb3.Start();
                            boton.BackColor = Color.Red;
                        }
                        else
                        {
                            if (cb3.ThreadState.Equals(ThreadState.WaitSleepJoin))
                            {
                                messagebox(ocupado);
                            }
                        }
                        break;
                    case "4":
                        if ((cb4.ThreadState.Equals(ThreadState.Stopped) || cb4.ThreadState.Equals(ThreadState.Aborted) || cb4.ThreadState.Equals(ThreadState.Unstarted)) && pantallaPrestamo.ShowDialog() == DialogResult.OK)
                        {
                            cb4 = new Thread(childref);
                            lblHora4.Text = "Hora inicial: " + DateTime.Now.ToShortTimeString();
                            btnEditar4.Enabled = true;
                            prestamo = false;
                            cb4.Start();
                            boton.BackColor = Color.Red;
                        }
                        else
                        {
                            if (cb4.ThreadState.Equals(ThreadState.WaitSleepJoin))
                            {
                                messagebox(ocupado);
                            }
                        }
                        break;
                    case "5":
                        ValidaEncargado validar = new ValidaEncargado(this,0);
                        if (validar.ShowDialog() == DialogResult.OK)
                        {
                            if ((cb5.ThreadState.Equals(ThreadState.Stopped) || cb5.ThreadState.Equals(ThreadState.Aborted) || cb5.ThreadState.Equals(ThreadState.Unstarted)) && pantallaPrestamo.ShowDialog() == DialogResult.OK)
                            {
                                cb5 = new Thread(childref);
                                lblHora5.Text = "Hora inicial: " + DateTime.Now.ToShortTimeString();
                                btnEditar5.Enabled = true;
                                prestamo = false;
                                cb5.Start();
                                btnCubiculo5.BackColor = Color.Red;
                            }
                        }
                        else
                        {
                            if (cb5.ThreadState.Equals(ThreadState.WaitSleepJoin))
                            {
                                messagebox(ocupado);
                            }
                        }
                        break;
                    case "6":
                        if ((cb6.ThreadState.Equals(ThreadState.Stopped) || cb6.ThreadState.Equals(ThreadState.Aborted) || cb6.ThreadState.Equals(ThreadState.Unstarted)) && pantallaPrestamo.ShowDialog() == DialogResult.OK)
                        {
                            cb6 = new Thread(childref);
                            lblHora6.Text = "Hora inicial: " + DateTime.Now.ToShortTimeString();
                            btnEditar6.Enabled = true;
                            prestamo = false;
                            cb6.Start();
                            boton.BackColor = Color.Red;
                        }
                        else
                        {
                            if (cb6.ThreadState.Equals(ThreadState.WaitSleepJoin))
                            {
                                messagebox(ocupado);
                            }
                        }
                        break;
                    case "7":
                        if ((cb7.ThreadState.Equals(ThreadState.Stopped) || cb7.ThreadState.Equals(ThreadState.Aborted) || cb7.ThreadState.Equals(ThreadState.Unstarted)) && pantallaPrestamo.ShowDialog() == DialogResult.OK)
                        {
                            cb7 = new Thread(childref);
                            lblHora7.Text = "Hora inicial: " + DateTime.Now.ToShortTimeString();
                            btnEditar7.Enabled = true;
                            prestamo = false;
                            cb7.Start();
                            boton.BackColor = Color.Red;
                        }
                        else
                        {
                            if (cb7.ThreadState.Equals(ThreadState.WaitSleepJoin))
                            {
                                messagebox(ocupado);
                            }
                        }
                        break;
                    case "8":
                        if ((cb8.ThreadState.Equals(ThreadState.Stopped) || cb8.ThreadState.Equals(ThreadState.Aborted) || cb8.ThreadState.Equals(ThreadState.Unstarted)) && pantallaPrestamo.ShowDialog() == DialogResult.OK)
                        {
                            cb8 = new Thread(childref);
                            lblHora8.Text = "Hora inicial: " + DateTime.Now.ToShortTimeString();
                            btnEditar8.Enabled = true;
                            prestamo = false;
                            cb8.Start();
                            boton.BackColor = Color.Red;
                        }
                        else
                        {
                            if (cb8.ThreadState.Equals(ThreadState.WaitSleepJoin))
                            {
                                messagebox("El cubiculo INEGI ya está ocupado");
                            }
                        }
                        break;
                }
                #endregion
            }
            else
            {
                MessageBox.Show("Ingresa un numero de control valido");
            }
        }

        private void Timer()
        {
            PrestamoNodo actual;
            try
            {
                Thread.Sleep(tiempo);
                string mensaje = cons.insertaPrestamoCubiculo(cabeza.personas, cabeza.cubiculo, cabeza.entrada, cabeza.prestamo, cabeza.id_alumno, cabeza.id_encargado, cabeza.carrera);
                actual = cabeza;
                cabeza = cabeza.sig;
                if (!mensaje.Equals(""))
                {
                    MessageBox.Show(mensaje, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2, MessageBoxOptions.ServiceNotification);
                }

                if (actual.cubiculo != 8)
                {
                    MessageBox.Show("Se acabo el tiempo del cubiculo " + actual.cubiculo, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2, MessageBoxOptions.ServiceNotification);
                    switch (actual.cubiculo)
                    {
                        case 1:
                            btnCubiculo1.BackColor = Color.YellowGreen;
                            this.Invoke(new Action(() => { inicialbl(actual.cubiculo); }));
                            break;
                        case 2:
                            btnCubiculo2.BackColor = Color.YellowGreen;
                            this.Invoke(new Action(() => { inicialbl(actual.cubiculo); }));
                            break;
                        case 3:
                            btnCubiculo3.BackColor = Color.YellowGreen;
                            this.Invoke(new Action(() => { inicialbl(actual.cubiculo); }));
                            break;
                        case 4:
                            btnCubiculo4.BackColor = Color.YellowGreen;
                            this.Invoke(new Action(() => { inicialbl(actual.cubiculo); }));
                            break;
                        case 5:
                            btnCubiculo5.BackColor = Color.Brown;
                            this.Invoke(new Action(() => { inicialbl(actual.cubiculo); }));
                            break;
                        case 6:
                            btnCubiculo6.BackColor = Color.YellowGreen;
                            this.Invoke(new Action(() => { inicialbl(actual.cubiculo); }));
                            break;
                        case 7:
                            btnCubiculo7.BackColor = Color.YellowGreen;
                            this.Invoke(new Action(() => { inicialbl(actual.cubiculo); }));
                            break;
                    }
                }
                else
                {
                    MessageBox.Show("Se acabo el tiempo del cubiculo INEGI","Aviso",MessageBoxButtons.OK,MessageBoxIcon.Exclamation,MessageBoxDefaultButton.Button2, MessageBoxOptions.DefaultDesktopOnly);
                    btnCubiculo8.BackColor = Color.YellowGreen;
                    this.Invoke(new Action(() => { inicialbl(actual.cubiculo); }));
                }
                
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void txtControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

     

        private void mostrarNombre(Graphics a)
        {
            

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void modificarCubiculos(object sender, EventArgs e)
        {
            ValidaEncargado confirma = new ValidaEncargado(this,0);
            if (confirma.ShowDialog() == DialogResult.OK)
            {
                Button boton = (Button)sender;
                int cubiculo = Convert.ToInt32(boton.Name.Substring(boton.Name.Length - 1));
                Modificar_Cubiculo modificar = new Modificar_Cubiculo(this, cubiculo);
                modificar.ShowDialog();
            }
        }

        private void cancelarHilos(object sender, EventArgs e)
        {
            Button boton = (Button)sender;

            int cubiculo = Convert.ToInt32(boton.Name.Substring(boton.Name.Length - 1));
            bool activo = false;

            switch(cubiculo)
            {
                case 1:
                    if (cb1.ThreadState == ThreadState.WaitSleepJoin)
                        activo = true;
                    else
                        activo = false;
                    break;
                case 2:
                    if (cb2.ThreadState == ThreadState.WaitSleepJoin)
                        activo = true;
                    else
                        activo = false;
                    break;
                case 3:
                    if (cb3.ThreadState == ThreadState.WaitSleepJoin)
                        activo = true;
                    else
                        activo = false;
                    break;
                case 4:
                    if (cb4.ThreadState == ThreadState.WaitSleepJoin)
                        activo = true;
                    else
                        activo = false;
                    break;
                case 5:
                    if (cb5.ThreadState == ThreadState.WaitSleepJoin)
                        activo = true;
                    else
                        activo = false;
                    break;
                case 6:
                    if (cb6.ThreadState == ThreadState.WaitSleepJoin)
                        activo = true;
                    else
                        activo = false;
                    break;
                case 7:
                    if (cb7.ThreadState == ThreadState.WaitSleepJoin)
                        activo = true;
                    else
                        activo = false;
                    break;
                case 8:
                    if (cb8.ThreadState == ThreadState.WaitSleepJoin)
                        activo = true;
                    else
                        activo = false;
                    break;
            }

            if (activo)
            {
                ValidaEncargado confirma = new ValidaEncargado(this,cubiculo);
                if(confirma.ShowDialog() == DialogResult.OK)
                {
                    cancelaPrestamo(cubiculo);
                    registraPrestamo(cubiculo);
                }
            }
            else
            {
                if (cubiculo == 8)
                {
                    MessageBox.Show("El cubiculo INEGI está libre.");
                }
                else
                {
                    MessageBox.Show("El cubiculo " + cubiculo + " está libre.");
                }
            }
        }

        private void tmiEncargados_Click(object sender, EventArgs e)
        {
            ValidaEncargado validar = new ValidaEncargado(this,0);
            if (validar.ShowDialog() == DialogResult.OK)
            {
                Administra_Encargados encargado = new Administra_Encargados(this);
                encargado.ShowDialog();
            }
        }

        private void txtControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSiguiente_Click(this, new EventArgs());
            }
        }

        private void txtSugerencia_TextChanged(object sender, EventArgs e)
        {
            if (txtSugerencia.Text == "")
            {
                btnEnviarSugerencia.Enabled = false;
            }
            else
            {
                btnEnviarSugerencia.Enabled = true;
            }
        }

        private void tmiReportes_Click_1(object sender, EventArgs e)
        {
            ValidaEncargado validar = new ValidaEncargado(this,0);
            if (validar.ShowDialog() == DialogResult.OK)
            {
                Reportes rep = new Reportes(this);
                rep.ShowDialog();
            }
        }

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            eliminaIDAlumno();
        }
    }
}