DROP DATABASE CUBICULOS_BIBLIOTECA

CREATE DATABASE CUBICULOS_BIBLIOTECA

USE Cubiculos_biblioteca

USE MASTER

CREATE TABLE T_CARRERAS
(
	id_carrera int not null,
	nombre_carrera varchar(MAX) not null
	PRIMARY KEY (id_carrera)
)

CREATE TABLE T_ALUMNOS
(
	id_alumno varchar(MAX) not null,
	apellido_paterno varchar(MAX) null,
	apellido_materno varchar(MAX) null,
	nombre_alumno varchar(MAX) null,
	carrera_alumno int null,
	FOREIGN KEY (carrera_alumno) references T_CARRERAS(id_carrera)
)

CREATE TABLE T_BITACORA
(
	id_cubiculo int not null,
	num_personas_b int not null,
	carrera_bitacora varchar(MAX) not null,
	cubiculo_bitacora int not null,
	fecha_bitacora date not null
)

CREATE TABLE T_ENCARGADOS
(
	id_encargado int not null,
	nombre_encargado varchar(MAX) not null,
	contraseņa varchar(15) not null,
	nivel_encargado int not null

	PRIMARY KEY (id_encargado)
)

CREATE TABLE T_CUBICULOS
(
	id_cubiculo int identity not null,
	fecha_cubiculo date not null,
	num_personas int not null,
	num_cubiculo int not null,
	entrada_cubiculo varchar(5) not null,
	salida_cubiculo varchar(5) not null,
	marcador_borrador char not null,
	id_alumno varchar(MAX) not null,
	id_encargado int not null,
	carrera_cubiculos varchar(MAX) not null

	PRIMARY KEY (id_cubiculo),
	FOREIGN KEY (id_encargado) references T_ENCARGADOS(id_encargado)
)

CREATE TABLE T_SUGERENCIAS
(
id_sugerencia int identity not null,
sugerencia varchar(MAX) not null
PRIMARY KEY (id_sugerencia)
)

/*TRIGGERS*/

CREATE Trigger [dbo].[Inserta_bitacora] on [dbo].[T_Cubiculos]
after insert
as
begin

insert into T_Bitacora

Select id_cubiculo, num_personas, carrera_cubiculos,num_cubiculo, fecha_cubiculo from inserted

end

/*INSERTA VISTAS*/

CREATE VIEW V_Alumnos
as
SELECT	dbo.T_ALUMNOS.id_alumno, dbo.T_ALUMNOS.nombre_alumno, dbo.T_CARRERAS.nombre_carrera
FROM    dbo.T_ALUMNOS INNER JOIN dbo.T_CARRERAS
ON dbo.T_ALUMNOS.carrera_alumno = dbo.T_CARRERAS.id_carrera


/*INSERTA ADMIN*/

Insert into T_ENCARGADOS values (1019,'Rigoberto Duarte', 12345, 1)

INSERT INTO T_ALUMNOS values (14330539, 'DUARTE', 'GONZALI', 'RIGOBERTO', 10)

/*INSERTA CARRERAS*/

Insert into T_CARRERAS values (34,'ING. BIOMEDICA')
Insert into T_CARRERAS values (33,'ING. EN INFORMATICA')
Insert into T_CARRERAS values (35,'ING. IND. 100 INGLES')
Insert into T_CARRERAS values (6,'INGENIERIA ELECTRICA')
Insert into T_CARRERAS values (11,'INGENIERIA ELECTRONICA')
Insert into T_CARRERAS values (32,'INGENIERIA EN GESTION EMPRESARIAL')
Insert into T_CARRERAS values (10,'INGENIERIA EN SISTEMAS COMPUTACIONALES')
Insert into T_CARRERAS values (8,'INGENIERIA INDUSTRIAL')
Insert into T_CARRERAS values (5,'INGENIERIA MECANICA')
Insert into T_CARRERAS values (31,'INGENIERIA MECATRONICA')
Insert into T_CARRERAS values (16,'LICENCIATURA EN ADMINISTRACION')
Insert into T_CARRERAS values (24,'MAESTRIA EN ADMINISTRACION')
Insert into T_CARRERAS values (28,'MAESTRIA EN CIENCIAS DE LA COMPUTACION')
Insert into T_CARRERAS values (27,'MAESTRIA EN INGENIERIA ELECTRICA')
Insert into T_CARRERAS values (29,'MAESTRIA EN INGENIERIA INDUSTRIAL')

SELECT* FROM T_ENCARGADOS

Select* from T_ALUMNOS

SELECT* from T_CARRERAS

SELECT* FROM T_BITACORA

SELECT* FROM T_CUBICULOS

SELECT* FROM T_SUGERENCIAS